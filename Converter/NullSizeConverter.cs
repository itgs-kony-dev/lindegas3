using System;
using System.Globalization;

using Xamarin.Forms;

namespace LindeGas3
{
	public class NullSizeConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (!(value is bool))
				return parameter ?? 0;

			var boolValue = (bool)value;

			return boolValue ? 24 : 0;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return false;
			//throw new NotImplementedException();
		}
	}
}

