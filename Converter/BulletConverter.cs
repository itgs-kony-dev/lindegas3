using System;
using System.Globalization;

using Xamarin.Forms;

namespace LindeGas3
{
	public class BulletConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value == null || !(value is int))
				return null;

			//string a = "⚫";

			/*string text = "";

			for (int i = 0; i <= (int)value; i++)
				text += "\u26AA ";//\u26AB \u25CF  \u23FA \u26AB 

			for (int i = (int)value + 1; i < 5; i++)
				text += "\u25CB ";  // u25CB u25E6 u26AC

			return text;*/
			StackLayout stackLayout = new StackLayout { Orientation = StackOrientation.Horizontal, HorizontalOptions = LayoutOptions.FillAndExpand, Padding=8 };

			if ((int)value == 0)
					return stackLayout;

			for (int i = 0; i < (int)value; i++)
				stackLayout.Children.Add(new Image { Source = ImageSource.FromResource("bullet"), WidthRequest=22, HeightRequest=22, Margin=1 });

			/*for (int i = (int)value + 1; i <= 3; i++)
				stackLayout.Children.Add(new Image { Source = ImageSource.FromResource("emptybullet") });*/

			return stackLayout;
			
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return false;
			//throw new NotImplementedException();
		}
	}
}

