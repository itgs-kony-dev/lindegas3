using System;
using System.Globalization;

using Xamarin.Forms;

namespace LindeGas3
{
	public class GasSelectedConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (!(value is bool))
				return Color.FromHex("#367a99");

			var boolValue = (bool)value;

			return boolValue ? Color.FromHex("#aadce8") : Color.FromHex("#367a99");
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return false;
			//throw new NotImplementedException();
		}
	}
}

