using System;
using System.Globalization;

using Xamarin.Forms;

namespace LindeGas3
{
	public class ColorConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (!(value is bool))
				return null;

			var boolValue = (bool)value;

			return boolValue ? Color.FromHex("#400000FF") : Color.FromHex("#40FFFFFF");
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return false;
			//throw new NotImplementedException();
		}
	}
}

