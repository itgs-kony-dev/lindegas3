using System;
using System.Globalization;

using Xamarin.Forms;

namespace LindeGas3
{
	public class TranslationConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value == null || !(value is string))
				return null;

			string text = "";

			if ((value as string) == "S")
				text = "Surface";
			if ((value as string) == "C")
				text = "CrossSection";
			
			return text;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return false;
			//throw new NotImplementedException();
		}
	}
}

