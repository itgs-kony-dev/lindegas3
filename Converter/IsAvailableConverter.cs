using System;
using System.Globalization;

using Xamarin.Forms;

namespace LindeGas3
{
	public class IsAvailableConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value == null)
				return false;
			if (value is int && ((int)value) == 0)
				return false;
			if (value is double && ((double)value) <= double.Epsilon)
				return false;
			if (value is Gas)
			{
				if (string.IsNullOrEmpty(((Gas)value).Name))
					return false;
				if (((Gas)value).SelectedList == "Gases2")
					return false;
			}

			return true;		
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return false;
			//throw new NotImplementedException();
		}
	}
}

