using System;
using System.Globalization;

using Xamarin.Forms;

namespace LindeGas3
{
	public class MatProcColorConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (!(value is bool))
				return null;

			var boolValue = (bool)value;

			return boolValue ? Color.White : Color.FromHex("#5984a4");
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return false;
			//throw new NotImplementedException();
		}
	}
}

