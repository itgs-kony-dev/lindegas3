using System;
using System.Collections.Generic;
using System.Linq;

using Xamarin.Forms;

namespace LindeGas3
{
	public partial class ProcessPage : ContentPage
	{
		public Metal Material { get; set; }

		IEnumerable<Process> processes;

		public ProcessPage()
		{
			InitializeComponent();

			Title = "Process";

			this.processes = App.Processes;
		}

		public void OnGMAWSelected(object sender, EventArgs args)
		{
			Navigation.PushAsync(new LindeGas3Page() { Material = Material, Process = this.processes.FirstOrDefault(x => x.Name == "GMAW") });
		}

		public void OnGTAWSelected(object sender, EventArgs args)
		{
			Navigation.PushAsync(new LindeGas3Page() { Material = Material, Process = this.processes.FirstOrDefault(x => x.Name == "GTAW") });
		}

	}
}
