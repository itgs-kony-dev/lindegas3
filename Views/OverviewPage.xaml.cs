﻿using System;
using System.Collections.Generic;
using System.Linq;

using Xamarin.Forms;

namespace LindeGas3
{
	public partial class OverviewPage : LindeGasPage
	{
		public OverviewPage()
		{
			InitializeComponent();

			NavigationPage.SetHasNavigationBar(this, false);


			this.aluMatProcMV = new MatProcVM { Material = App.Metals.FirstOrDefault(x => x.Name == "Aluminium") };
			this.aluMatProcMV.OnSelected += this.OnSelectionChanged;

			this.mildSteelMatProcMV = new MatProcVM { Material = App.Metals.FirstOrDefault(x => x.Name == "Mild steel") };
			this.mildSteelMatProcMV.OnSelected += this.OnSelectionChanged;

			this.stainlessSteelMatProcMV = new MatProcVM { Material = App.Metals.FirstOrDefault(x => x.Name == "Stainless steel") };
			this.stainlessSteelMatProcMV.OnSelected += this.OnSelectionChanged;

			this.aluminiumView.BindingContext = aluMatProcMV;
			this.mildSteelView.BindingContext = mildSteelMatProcMV;
			this.stainlessSteelView.BindingContext = stainlessSteelMatProcMV;

			GMAW = App.Processes.FirstOrDefault(x => x.Name == "GMAW");
			GTAW = App.Processes.FirstOrDefault(x => x.Name == "GTAW");
		}
	}
}
