using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace LindeGas3
{
	public partial class UnselectedCell : ViewCell
	{
		public UnselectedCell()
		{
			InitializeComponent();

			this.Tapped += (object sender, EventArgs e) =>
			{
				if (BindingContext is Gas)
				{
					Gas gas = (Gas)BindingContext;
					gas.Cell = this;

					backgroundFrame.BackgroundColor = gas.IsSelected ? Color.FromRgba(0, 0, 255, 64) : Color.FromRgba(255, 255, 255, 64);
				}
			};
		}

		public void SetBackgroundColor()
		{
			if (BindingContext is Gas)
			{
				Gas gas = (Gas)BindingContext;

				backgroundFrame.BackgroundColor = gas.IsSelected ? Color.FromRgba(0, 0, 255, 64) : Color.FromRgba(255, 255, 255, 64);
			}
		}


	}
}
