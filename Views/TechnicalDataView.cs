using System;
using System.Collections.Generic;

using Xamarin.Forms;

//using SkiaSharp;

namespace LindeGas3
{
	public partial class TechnicalDataView : ContentView
	{
		public TechnicalDataView()
		{
			InitializeComponent();
		}

		protected override void OnBindingContextChanged()
		{
			base.OnBindingContextChanged();

			if (BindingContext is TechnicalData)
			{
				TechnicalData td = (TechnicalData)BindingContext;

				this.thicknessRange.IsVisible = td.ThicknessRangeTo > 0;
			}
			else
				this.thicknessRange.IsVisible = false;
		}
	}
}

