using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

using Xamarin;
using Xamarin.Forms;

using Urho;
using Urho.Forms;

using FormsSample;

using App2Heaven;

namespace LindeGas3
{
	public partial class LindeGas3Page : ContentPage
	{
		public Metal Material { get; set; }

		public Process Process { get; set; }

		IEnumerable<ICombination> combos;
		//UrhoSurface urho;
		Charts charts;
		//Metal selectedMetal;

		//Process selectedProcess;

		Gas selectedGas;
		Gas selectedGas2;

		//bool dontSelect;

		public LindeGas3Page()
		{
			InitializeComponent();

			Title = "Gas";

			combos = App.Emissions;
			combos = combos.Union(App.TechnicalData);
			combos = combos.Union(App.CrossSections);
			combos = combos.Union(App.WikiData);

			/*IEnumerable<Metal> currentMaterials = combos
				.GroupBy(x => x.Metal)
				.Select(x => x.First())
				.Select(x => x.Metal)
				.Join(
				App.RootObject.Metals,
					combo => combo,
					metal => metal.Name,
					(combo, metal) => metal
				)
				.OrderBy(x => x.Name)
				.ToList();

			IEnumerable<Process> currentProcesses = combos
				.GroupBy(x => x.Process)
				.Select(x => x.First())
				.Select(x => x.Process)
				.Join(
					App.RootObject.Processes,
					combo => combo,
					process => process.Name,
					(combo, process) => process
				)
				.OrderBy(x => x.Name)
				.ToList();*/

			//this.materials.ItemsSource = currentMaterials;
			//this.process.ItemsSource = currentProcesses;

			//urho = new UrhoSurface { HorizontalOptions = LayoutOptions.FillAndExpand, VerticalOptions = LayoutOptions.FillAndExpand };


			/*Slider slider1r = new Slider(0f, 1f, 1f);
			Slider slider1g = new Slider(0f, 1f, 1f);
			Slider slider1b = new Slider(0f, 1f, 1f);
			Slider slider2r = new Slider(0f, 1f, 0.0f);
			Slider slider2g = new Slider(0f, 1f, 0.05f);
			Slider slider2b = new Slider(0f, 1f, 0.2f);

			slider1r.ValueChanged += (sender, e) => charts.SetValue1r((float) e.NewValue);
			slider1g.ValueChanged += (sender, e) => charts.SetValue1g((float) e.NewValue);
			slider1b.ValueChanged += (sender, e) => charts.SetValue1b((float) e.NewValue);
			slider2r.ValueChanged += (sender, e) => charts.SetValue2r((float) e.NewValue);
			slider2g.ValueChanged += (sender, e) => charts.SetValue2g((float) e.NewValue);
			slider2b.ValueChanged += (sender, e) => charts.SetValue2b((float) e.NewValue);

			StackLayout stackLayout = new StackLayout
			{
				Children = {
					urho,
					slider1r,
					slider1g,
					slider1b,
					slider2r,
					slider2g,
					slider2b
				}
			};
*/

			//this.objectView.Content = urho;
			//this.urho3DView = urho;
		}

		protected override async void OnAppearing()
		{
			base.OnAppearing();

			this.urho3DFrame.IsVisible = false;
			this.urho3DFrame.Opacity = 100;
			//charts = await urho.Show<Charts>(new ApplicationOptions(assetsFolder: null) { Orientation = ApplicationOptions.OrientationType.LandscapeAndPortrait });

			IEnumerable<Gas> currentGases = combos?
					.Where(x => x.Metal == this.Material.Name)
					.Where(x => x.Process == this.Process.Name)
					.GroupBy(x => x.Gas)
					.Select(x => x.First())
					.Select(x => x.Gas)
					.Join(
						App.Gases,
						combo => combo,
						process => process.Name,
						(combo, gas) => gas
					)
					.OrderBy(x => x.Name)
					.ToList();

			this.gases.ItemsSource = currentGases;

			Title = Material.Name + " / " + Process.Name;
			//this.selectedMaterialLabel.Text = Material.Name;
			//this.selectedProcessLabel.Text = Process.Name;
		}

		/*public void OnMaterialSelected(object sender, SelectedItemChangedEventArgs args)
		{
			if (args.SelectedItem is Metal)
			{
				this.selectedMetal = (Metal)args.SelectedItem;

				IEnumerable<Process> currentProcesses = combos?
				.Where(x => x.Metal == this.selectedMetal?.Name)
				.GroupBy(x => x.Process)
				.Select(x => x.First())
				.Select(x => x.Process)
				.Join(
					App.RootObject.Processes,
					combo => combo,
					process => process.Name,
					(combo, process) => process
				)
				.OrderBy(x => x.Name)
				.ToList();

				this.process.ItemsSource = currentProcesses;
				this.process.IsVisible = true;

				this.materials.IsVisible = false;

				this.selectedMaterialView.IsVisible = true;
				this.selectedMaterialLabel.Text = this.selectedMetal.Name;
			}
		}

		public void OnProcessSelected(object sender, SelectedItemChangedEventArgs args)
		{
			if (args.SelectedItem is Process)
			{
				this.selectedProcess = (Process)args.SelectedItem;

				IEnumerable<Gas> currentGases = combos?
					.Where(x => x.Metal == this.selectedMetal?.Name)
					.Where(x => x.Process == this.selectedProcess?.Name)
					.GroupBy(x => x.Gas)
					.Select(x => x.First())
					.Select(x => x.Gas)
					.Join(
						App.RootObject.Gases,
						combo => combo,
						process => process.Name,
						(combo, gas) => gas
					)
					.OrderBy(x => x.Name)
					.ToList();

				this.gases.ItemsSource = currentGases;
				this.gases.IsVisible = true;

				this.process.IsVisible = false;

				this.selectedProcessView.IsVisible = true;
				this.selectedProcessLabel.Text = this.selectedProcess.Name;
			}

		}*/

		public void OnGasSelected(object sender, SelectedItemChangedEventArgs args)
		{
			if (args.SelectedItem == null)
				return;

			if (this.selectedGas != null)
			{
				if (this.selectedGas == args.SelectedItem)
				{
					this.selectedGas.IsSelected = false;
					this.selectedGas = null;

					if (this.selectedGas2 != null)
					{
						this.selectedGas = this.selectedGas2;
						this.selectedGas2 = null;
					}
				}
				else
				{
					if (this.selectedGas2 != null && this.selectedGas2 != args.SelectedItem)
					{
						this.selectedGas2.IsSelected = false;
						this.selectedGas2.Cell?.SetBackgroundColor();
						this.selectedGas2 = null;
					}

					this.selectedGas2 = (Gas)args.SelectedItem;
					this.selectedGas2.IsSelected = !this.selectedGas2.IsSelected;
					this.selectedGas2 = !this.selectedGas2.IsSelected ? null : this.selectedGas2;
				}
			}
			else
			{
				this.selectedGas = (Gas)args.SelectedItem;
				this.selectedGas.IsSelected = !this.selectedGas.IsSelected;
				this.selectedGas = !this.selectedGas.IsSelected ? null : this.selectedGas;

			}

			this.gases.SelectedItem = null;

			//this.emissionView.BindingContext = new Emission[][] { emissions.ToArray() };

			ShowData();

			ShowData2();

			this.dataScrollView.IsVisible = this.selectedGas != null || this.selectedGas2 != null;

		}

		void ShowData()
		{
			TechnicalData technicalData = App.TechnicalData
				.FirstOrDefault(x => x.Metal == this.Material.Name && x.Process == this.Process.Name && x.Gas == this.selectedGas?.Name);

			CrossSection crossSection = App.CrossSections
				.FirstOrDefault(x => x.Metal == this.Material.Name && x.Process == this.Process.Name && x.Gas == this.selectedGas?.Name && x.Type.ToUpper() == "C");

			CrossSection surface = App.CrossSections
				.FirstOrDefault(x => x.Metal == this.Material.Name && x.Process == this.Process.Name && x.Gas == this.selectedGas?.Name && x.Type.ToUpper() == "S");

			WikiData wikiData = App.WikiData
				.FirstOrDefault(x => x.Metal == this.Material.Name && x.Process == this.Process.Name && x.Gas == this.selectedGas?.Name);

			Emission[] emissions = App.Emissions
				.Where(x => x.Metal == this.Material.Name && x.Process == this.Process.Name && x.Gas == this.selectedGas?.Name)
									  .ToList().ToArray();

			Model3DScan model = App.Model3DScan
									  .FirstOrDefault(x => x.Metal == this.Material.Name && x.Process == this.Process.Name && x.Gas == this.selectedGas?.Name);

			TechnicalDataView tdv = new TechnicalDataView();

			if (technicalData != null)
			{
				tdv.BindingContext = technicalData;
				tdv.IsVisible = true;
			}
			else
				tdv.IsVisible = false;

			this.detailsView.Content = tdv;

			if (technicalData != null)
			{
				this.details2View.Content = new ContentView
				{
					Content = new ScrollView { Orientation = ScrollOrientation.Vertical, Content = new Label { TextColor = Xamarin.Forms.Color.White, FontSize = 16, Text = technicalData.Description } },
					Padding = new Thickness(8)
				};
			}
			else
			{
				this.details2View.Content = null;
			}

			this.crossSectionImage.Source = ImageSource.FromResource(crossSection?.Filename ?? " ");

			this.surfaceImage.Source = ImageSource.FromResource(surface?.Filename ?? " ");

			try
			{
				if (wikiData != null && !string.IsNullOrWhiteSpace(wikiData.Filename))
				{
					VideoView2 videoView = new VideoView2 { FileName = wikiData.Filename, WidthRequest = 320, HeightRequest = 320, HorizontalOptions = LayoutOptions.StartAndExpand, VerticalOptions = LayoutOptions.StartAndExpand };

					videoView.GestureRecognizers.Add(new TapGestureRecognizer
					{
						Command = new Command((obj) =>
						{
							OnVideoViewTapped(videoView, null);
						})
					}); 

					this.videoViewLayout.Content = videoView;
				}
				else
					this.videoViewLayout.Content = null;
			}
			catch (Exception exception)
			{
			}

			if (emissions != null && emissions.Length > 0)
				this.emissionView.Content = new SkiaView { BindingContext = new Emission[][] { emissions.ToArray() }, WidthRequest = 320, HeightRequest = 320 };
			else
				this.emissionView.Content = null;

			this.preview3DImage.Source = ImageSource.FromResource(model?.Preview ?? " ");

			if (this.selectedGas2 == null)
			{
				grid.Children.Add(this.crossSectionView, 0, 1);
				grid.Children.Add(this.crossSection2View, 0, 3);
				grid.Children.Add(this.surfaceView, 1, 1);
				grid.Children.Add(this.surface2View, 1, 5);
				grid.Children.Add(this.videoViewLayout, 0, 2);
				grid.Children.Add(this.videoViewLayout2, 0, 2);
				grid.Children.Add(this.videoView2Layout, 0, 4);
				grid.Children.Add(this.videoView2Layout2, 0, 4);

				grid.Children.Add(this.emission2View, 1, 4);
				grid.Children.Add(this.emission2View2, 1, 4);
				grid.Children.Add(this.emissionView, 1, 2);
				grid.Children.Add(this.emissionView2, 1, 2);

				grid.Children.Add(this.preview3D2View, 0, 5);
				grid.Children.Add(this.preview3DView, 1, 3);

				frame03.IsVisible = false;
				frame04.IsVisible = false;
				frame05.IsVisible = false;
				//frame13.IsVisible = false;
				frame14.IsVisible = false;
				frame15.IsVisible = false;
			}
			else
			{
				grid.Children.Add(this.crossSectionView, 1, 1);
				grid.Children.Add(this.crossSection2View, 0, 1);
				grid.Children.Add(this.surfaceView, 1, 2);
				grid.Children.Add(this.surface2View, 0, 2);
				grid.Children.Add(this.videoViewLayout, 1, 3);
				grid.Children.Add(this.videoViewLayout2, 1, 3);
				grid.Children.Add(this.videoView2Layout, 0, 3);
				grid.Children.Add(this.videoView2Layout2, 0, 3);

				grid.Children.Add(this.emission2View, 0, 4);
				grid.Children.Add(this.emission2View2, 0, 4);
				grid.Children.Add(this.emissionView, 1, 4);
				grid.Children.Add(this.emissionView2, 1, 4);

				grid.Children.Add(this.preview3D2View, 0, 5);
				grid.Children.Add(this.preview3DView, 1, 5);

				frame03.IsVisible = true;
				frame04.IsVisible = true;
				frame05.IsVisible = true;
				//frame13.IsVisible = true;
				frame14.IsVisible = true;
				frame15.IsVisible = true;
			}

			if (this.selectedGas != null)
				this.selectedGasLabel.Text = this.selectedGas.Name;
			else
				this.selectedGasLabel.Text = "";
		}

		void ShowData2()
		{
			TechnicalData technicalData = App.TechnicalData
				.FirstOrDefault(x => x.Metal == this.Material.Name && x.Process == this.Process.Name && x.Gas == this.selectedGas2?.Name);

			CrossSection crossSection = App.CrossSections
				.FirstOrDefault(x => x.Metal == this.Material.Name && x.Process == this.Process.Name && x.Gas == this.selectedGas2?.Name && x.Type.ToUpper() == "C");

			CrossSection surface = App.CrossSections
				.FirstOrDefault(x => x.Metal == this.Material.Name && x.Process == this.Process.Name && x.Gas == this.selectedGas2?.Name && x.Type.ToUpper() == "S");

			WikiData wikiData = App.WikiData
				.FirstOrDefault(x => x.Metal == this.Material.Name && x.Process == this.Process.Name && x.Gas == this.selectedGas2?.Name);

			Emission[] emissions = App.Emissions
				.Where(x => x.Metal == this.Material.Name && x.Process == this.Process.Name && x.Gas == this.selectedGas2?.Name)
				.ToList().ToArray();

			Model3DScan model = App.Model3DScan
									  .FirstOrDefault(x => x.Metal == this.Material.Name && x.Process == this.Process.Name && x.Gas == this.selectedGas2?.Name);

			TechnicalDataView tdv = new TechnicalDataView();

			if (technicalData != null)
			{
				tdv.BindingContext = technicalData;
				tdv.IsVisible = true;
			}
			else
				tdv.IsVisible = false;

			if (selectedGas2 != null)
				this.details2View.Content = tdv;
			/*else if(technicalData != null)
			{
				this.details2View.Content = new ContentView
				{
					Content = new Label { TextColor = Color.White, FontSize = 16, Text = technicalData.Description },
					Padding = new Thickness(8)
				};
			}*/

			this.crossSection2Image.Source = ImageSource.FromResource(crossSection?.Filename ?? " ");

			this.surface2Image.Source = ImageSource.FromResource(surface?.Filename ?? " ");

			try
			{
				if (wikiData != null && !string.IsNullOrWhiteSpace(wikiData.Filename))
					this.videoView2Layout.Content = new VideoView2 { FileName = wikiData.Filename, WidthRequest = 320, HeightRequest = 320, HorizontalOptions = LayoutOptions.Start };
				else
					this.videoView2Layout.Content = null;
			}
			catch (Exception exception)
			{
			}

			if (emissions != null && emissions.Length > 0)
				this.emission2View.Content = new SkiaView { BindingContext = new Emission[][] { emissions.ToArray() }, WidthRequest = 320, HeightRequest = 320 };
			else
				this.emission2View.Content = null;

			if (this.selectedGas2 != null)
				this.selectedGas2Label.Text = this.selectedGas2.Name;
			else
				this.selectedGas2Label.Text = "";

			this.preview3D2Image.Source = ImageSource.FromResource(model?.Preview ?? " ");
		}

		public void OnSelectedMaterialTapped(object sender, EventArgs args)
		{
			/*this.process.IsVisible = false;
			this.gases.IsVisible = false;
			this.materials.IsVisible = true;
			//this.materials.SelectedItem = this.selectedMetal;
			this.selectedProcessView.IsVisible = false;
			this.selectedProcess = null;
			this.process.SelectedItem = null;
			this.materials.SelectedItem = null;
			this.selectedGas = null;
			this.selectedGas2 = null;
			this.gases.SelectedItem = null;
			ClearView();
			*/

		}

		public void OnSelectedProcessTapped(object sender, EventArgs args)
		{
			/*
			this.gases.IsVisible = false;
			this.materials.IsVisible = false;
			this.process.IsVisible = true;
			//this.process.SelectedItem = this.selectedProcess;
			this.process.SelectedItem = null;
			this.materials.SelectedItem = null;
			this.selectedGas = null;
			this.selectedGas2 = null;
			this.gases.SelectedItem = null;
			ClearView();
			*/
		}

		void ClearView()
		{
			this.detailsView.Content = null;
			this.crossSectionImage.Source = null;
			this.videoViewLayout.Content = null;
			this.emissionView.Content = null;

			this.details2View.Content = null;
			this.crossSection2Image.Source = null;
			this.videoView2Layout.Content = null;
			this.emission2View.Content = null;

			this.dataScrollView.IsVisible = false;

		}

		public void OnCrossSectionTapped(object sender, EventArgs args)
		{
			Image image = new Image
			{
				Source = this.crossSectionImage.Source,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Aspect = Aspect.AspectFit,
				Margin = new Thickness(0, 0, 0, 64)              
			};

			image.GestureRecognizers.Add(new TapGestureRecognizer
			{
				Command = new Command(async => this.popupView.IsVisible = false)
			});

			this.popupView.Content = image;
			this.popupView.IsVisible = true;
		}

		public void OnCrossSectionTapped2(object sender, EventArgs args)
		{
			Image image = new Image
			{
				Source = this.crossSection2Image.Source,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Aspect = Aspect.AspectFit,
				Margin = new Thickness(0, 0, 0, 64)
			};

			image.GestureRecognizers.Add(new TapGestureRecognizer
			{
				Command = new Command(async => this.popupView.IsVisible = false)
			});

			this.popupView.Content = image;
			this.popupView.IsVisible = true;
		}

		public void OnSurfaceTapped(object sender, EventArgs args)
		{
			CrossSection surface = App.CrossSections
				.FirstOrDefault(x => x.Metal == this.Material.Name && x.Process == this.Process.Name && x.Gas == this.selectedGas?.Name && x.Type.ToUpper() == "S");

			Image image = new Image
			{
				Source = ImageSource.FromResource(surface?.Filename ?? " "),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Aspect = Aspect.AspectFit,
				Margin = new Thickness(0, 0, 0, 0)
			};

			image.GestureRecognizers.Add(new TapGestureRecognizer
			{
				Command = new Command(async => this.popupView.IsVisible = false)					
			});

			this.popupView.Content = image;
			this.popupView.IsVisible = true;
		}

		public void OnSurfaceTapped2(object sender, EventArgs args)
		{
			Image image = new Image
			{
				Source = this.surface2Image.Source,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Aspect = Aspect.AspectFit,
				Margin = new Thickness(0, 0, 0, 0)
			};

			image.GestureRecognizers.Add(new TapGestureRecognizer
			{
				Command = new Command(async => this.popupView.IsVisible = false)
			});

			this.popupView.Content = image;
			this.popupView.IsVisible = true;
		}

		public void OnVideoViewTapped(object sender, EventArgs args)
		{
			WikiData wikiData = App.WikiData
				.FirstOrDefault(x => x.Metal == this.Material.Name && x.Process == this.Process.Name && x.Gas == this.selectedGas?.Name);

			if (wikiData != null && !string.IsNullOrWhiteSpace(wikiData.Filename))
			{
				VideoView videoView = new VideoView { FileName = wikiData.Filename, WidthRequest = 640, HeightRequest = 480, HorizontalOptions = LayoutOptions.CenterAndExpand, VerticalOptions = LayoutOptions.CenterAndExpand };
			
				videoView.GestureRecognizers.Add(new TapGestureRecognizer
				{
					Command = new Command(async => this.popupView.IsVisible = false)
				});

				this.popupView.Content = videoView;
				this.popupView.IsVisible = true;
			}
		}

		public void OnVideoViewTapped2(object sender, EventArgs args)
		{
			WikiData wikiData = App.WikiData
				.FirstOrDefault(x => x.Metal == this.Material.Name && x.Process == this.Process.Name && x.Gas == this.selectedGas2?.Name);

			if (wikiData != null && !string.IsNullOrWhiteSpace(wikiData.Filename))
			{
				VideoView videoView = new VideoView { FileName = wikiData.Filename, WidthRequest = 640, HeightRequest = 480, HorizontalOptions = LayoutOptions.CenterAndExpand, VerticalOptions = LayoutOptions.CenterAndExpand };

				videoView.GestureRecognizers.Add(new TapGestureRecognizer
				{
					Command = new Command(async => this.popupView.IsVisible = false)
				});

				this.popupView.Content = videoView;
				this.popupView.IsVisible = true;
			}
		}

		public void OnEmissionViewTapped(object sender, EventArgs args)
		{
			Emission[] emissions = App.Emissions
				.Where(x => x.Metal == this.Material.Name && x.Process == this.Process.Name && x.Gas == this.selectedGas?.Name)
				.ToList().ToArray();

			SkiaView skiaView = new SkiaView { BindingContext = new Emission[][] { emissions.ToArray() }, WidthRequest = 320, HeightRequest = 320 };

			skiaView.GestureRecognizers.Add(new TapGestureRecognizer
			{
				Command = new Command(async => this.popupView.IsVisible = false)
			});

			this.popupView.Content = new ScrollView { Content = skiaView, Orientation = ScrollOrientation.Vertical };
			this.popupView.IsVisible = true;
		}

		public void OnEmissionViewTapped2(object sender, EventArgs args)
		{
			Emission[] emissions = App.Emissions
				.Where(x => x.Metal == this.Material.Name && x.Process == this.Process.Name && x.Gas == this.selectedGas2?.Name)
				.ToList().ToArray();

			SkiaView skiaView = new SkiaView { BindingContext = new Emission[][] { emissions.ToArray() }, WidthRequest = 320, HeightRequest = 320 };

			skiaView.GestureRecognizers.Add(new TapGestureRecognizer
			{
				Command = new Command(async => this.popupView.IsVisible = false)
			});

			this.popupView.Content = new ScrollView { Content = skiaView, Orientation = ScrollOrientation.Vertical };
			this.popupView.IsVisible = true;
		}

		public async void On3DViewTapped(object sender, EventArgs args)
		{
			//urho = new UrhoSurface { HorizontalOptions = LayoutOptions.FillAndExpand, VerticalOptions = LayoutOptions.FillAndExpand };

			Model3DScan model = App.Model3DScan
									  .FirstOrDefault(x => x.Metal == this.Material.Name && x.Process == this.Process.Name && x.Gas == this.selectedGas?.Name);

			this.urho3DFrame.IsVisible = true;

			if(charts == null)
				charts = await urho3DView.Show<Charts>(new ApplicationOptions(assetsFolder: null) { Orientation = ApplicationOptions.OrientationType.LandscapeAndPortrait });

			charts.SetModel(model.Filename, model.Material);


			//charts = await urho3DView.Show<Charts>(new ApplicationOptions(assetsFolder: null) { Orientation = ApplicationOptions.OrientationType.LandscapeAndPortrait });

			//this.popupView.Content = urho;
			//this.urho3DView.IsVisible = true;

			//await Task.Delay(1000).ContinueWith(async (arg) => charts = await urho.Show<Charts>(new ApplicationOptions(assetsFolder: null) { Orientation = ApplicationOptions.OrientationType.LandscapeAndPortrait }));

			//urho = new UrhoSurface { HorizontalOptions = LayoutOptions.FillAndExpand, VerticalOptions = LayoutOptions.FillAndExpand };

			//Test.Content = urho;
		}

		public async void On3D2ViewTapped(object sender, EventArgs args)
		{
			Model3DScan model = App.Model3DScan
									  .FirstOrDefault(x => x.Metal == this.Material.Name && x.Process == this.Process.Name && x.Gas == this.selectedGas2?.Name);

			this.urho3DFrame.IsVisible = true;

			if (charts == null)
				charts = await urho3DView.Show<Charts>(new ApplicationOptions(assetsFolder: null) { Orientation = ApplicationOptions.OrientationType.LandscapeAndPortrait });

			charts.SetModel(model.Filename, model.Material);
		}

		public void OnClose(object sender, EventArgs args)
		{
			this.popupView.IsVisible = false;
		}

		public void OnCloseUrho(object sender, EventArgs args)
		{
			this.urho3DFrame.IsVisible = false;
			//this.urh
			this.charts.UnsetModel();
			/*this.charts.Dispose();
			this.charts = null;*/
		}

		public void OnCollapseTapped(object sender, EventArgs args)
		{
			this.gases.IsVisible = !this.gases.IsVisible;

			this.gasesColumnDefinition.Width = new GridLength(this.gases.IsVisible ? 28 : 5, GridUnitType.Star);

			this.collapseLabel.Text = this.gases.IsVisible ? ">" : "<";
		}

		public void OnInfoTapped(object sender, EventArgs args)
		{
			this.popupView.Content = new Label { Text = "Base Material: mild steel (S355), Stainless (1.4301), Aluminum (Al5356) \nFiller Material: mild steel (G3Si1), Stainless (ER308LSI ), Aluminum (AlMg3) \n \nProcess: GMAW \nWire feed speed: 3.5 m/min (short\u2002\u2002arc) and 10 m/min (pulsed for Al, spray arc for steel) \nWire diameter: 1.2 mm \nStickout: 20 mm \nShielding gas flow rate: 15 l/min \nWelding Equipment: EWM Alpha Q552 Plus MM, Binzel MasterFeeder \n \nProcess: GTAW (no filler wire) \nCurrent: 100A, 200A \nShielding gas flow rate: 12 l/min \nWelding Equipment: EWM inverter 450 AC/DC-P and Binzel Abtig ", FontSize = 18f, TextColor = Xamarin.Forms.Color.White };
			this.popupView.IsVisible = true;
		}
	}
}

