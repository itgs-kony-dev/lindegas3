﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Linq;
using System.Diagnostics;
using System.Windows.Input;

using Xamarin;
using Xamarin.Forms;

using Plugin.Media.Abstractions;
using Microsoft.WindowsAzure.MobileServices.Files;

using System.Reflection;
using Microsoft.WindowsAzure.Storage.Blob;

namespace LindeGas3
{
	public partial class LindeGasPage : ContentPage
	{
		//protected Metal Material;
		//protected Process Process;
		protected Process GMAW;
		protected Process GTAW;
		string Emission = "O3";

		protected MatProcVM aluMatProcMV;
		protected MatProcVM mildSteelMatProcMV;
		protected MatProcVM stainlessSteelMatProcMV;

		IFileHelper fileHelper;

		public LindeGasPage()
		{
			InitializeComponent();

			this.fileHelper = this.fileHelper = DependencyService.Get<IFileHelper>();

			NavigationPage.SetHasNavigationBar(this, false);
		}

		protected override async void OnAppearing()
		{
			base.OnAppearing();

			BindingContext = App.tdVM;

			this.aluMatProcMV = new MatProcVM { Material = App.Metals.FirstOrDefault(x => x.Name == "Aluminium") };
			this.aluMatProcMV.MaterialSelected = App.tdVM.Material?.Name == "Aluminium";
			this.aluMatProcMV.GMAWSelected = App.tdVM.Material?.Name == "Aluminium" && App.tdVM.Process?.Name == "GMAW";
			this.aluMatProcMV.GTAWSelected = App.tdVM.Material?.Name == "Aluminium" && App.tdVM.Process?.Name == "GTAW";
			this.aluMatProcMV.OnSelected += this.OnSelectionChanged;

			this.mildSteelMatProcMV = new MatProcVM { Material = App.Metals.FirstOrDefault(x => x.Name == "Mild steel") };
			this.mildSteelMatProcMV.MaterialSelected = App.tdVM.Material?.Name == "Mild steel";
			this.mildSteelMatProcMV.GMAWSelected = App.tdVM.Material?.Name == "Mild steel" && App.tdVM.Process?.Name == "GMAW";
			this.mildSteelMatProcMV.GTAWSelected = App.tdVM.Material?.Name == "Mild steel" && App.tdVM.Process?.Name == "GTAW";
			this.mildSteelMatProcMV.HasCorgon = true;
			this.mildSteelMatProcMV.OnSelected += this.OnSelectionChanged;

			this.stainlessSteelMatProcMV = new MatProcVM { Material = App.Metals.FirstOrDefault(x => x.Name == "Stainless steel") };
			this.stainlessSteelMatProcMV.MaterialSelected = App.tdVM.Material?.Name == "Stainless steel";
			this.stainlessSteelMatProcMV.GMAWSelected = App.tdVM.Material?.Name == "Stainless steel" && App.tdVM.Process?.Name == "GMAW";
			this.stainlessSteelMatProcMV.GTAWSelected = App.tdVM.Material?.Name == "Stainless steel" && App.tdVM.Process?.Name == "GTAW";
			this.stainlessSteelMatProcMV.OnSelected += this.OnSelectionChanged;

			this.aluminiumView.BindingContext = aluMatProcMV;
			this.mildSteelView.BindingContext = mildSteelMatProcMV;
			this.stainlessSteelView.BindingContext = stainlessSteelMatProcMV;

			GMAW = App.Processes.FirstOrDefault(x => x.Name == "GMAW");
			GTAW = App.Processes.FirstOrDefault(x => x.Name == "GTAW");

			//ObservableCollection<TodoItem> items = await ItemManager<TodoItem>.Instance.GetItemsAsync(true);

			//ItemManager<Model3DScan>.Instance.Writeable = true;
			//ItemManager<Gas>.Instance.Writeable = true;
			//ItemManager<WikiData>.Instance.Writeable = true;

			//await ItemManager<TechnicalData>.Instance.SaveItemAsync(new TodoItem { Text = "Test" }, true);

			/*foreach (Model3DScan model in App.RootObject.Model3DScan)
			{
				await ItemManager<Model3DScan>.Instance.SaveItemAsync(model, true);
			}*/

			//ItemManager<Process>.Instance.Writeable = true;

			//await ItemManager<Process>.Instance.SaveItemAsync(new Process { Name = "Test Process" }, true);


			//var image = await Plugin.Media.CrossMedia.Current.PickPhotoAsync();

			/*ItemManager<CrossSection>.Instance.Writeable = true;

			await ItemManager<CrossSection>.Instance.SaveItemAsync(monkey, true);
			await ItemManager<CrossSection>.Instance.AddImage(monkey, image.Path);

			await ItemManager<CrossSection>.Instance.SyncAsync();*/


			/*IEnumerable<CrossSection> css = await ItemManager<CrossSection>.Instance.GetItemsAsync(true);

			foreach (CrossSection cs in css)
			{
				IEnumerable<MobileServiceFile> files = await ItemManager<CrossSection>.Instance.GetImageFiles(cs);

				if (files.Count() < 1)
				{
					await ItemManager<CrossSection>.Instance.AddImage(cs, image.Path);
					await ItemManager<CrossSection>.Instance.SyncAsync();
					//break;
				}
			}*/

			//Zweiter Ansatz für FileSync direkt über BlobContainer
			/*CloudBlobClient blobClient = App.storageAccount.CreateCloudBlobClient();
			CloudBlobContainer container = blobClient.GetContainerReference("mycontainer");
			CloudBlockBlob blockBlob = container.GetBlockBlobReference("myblob");
			await fileHelper.DownloadToFileAsync(blockBlob, fileHelper.FilePath + "/" + "test.png");*/

			await ItemManager<CrossSection>.Instance.SyncAsync();

			IEnumerable<CrossSection> css = await ItemManager<CrossSection>.Instance.GetItemsAsync(true);
			CrossSection cs = css.Last(x => x.Metal == "Mild steel" && x.Gas == "CORGON 5S2" && x.Process == "GMAW" && x.Type == "S");
			IEnumerable<MobileServiceFile> files = await ItemManager<CrossSection>.Instance.GetImageFiles(cs);
			//await ItemManager<CrossSection>.Instance.SyncAsync();

			if (App.TutState < 1)
			{
				await Task.Delay(1000);

				var b = this.tut01Image.Bounds;
				double oldX = b.X;
				b.X = this.Width / 4;

				await this.tut01Image.LayoutTo(b, 0);
				this.tut01Image.FadeTo(1, 500);
				b.X = oldX;
				await this.tut01Image.LayoutTo(b, 1000, Easing.BounceOut);
			}
		}

		protected virtual void Handle_Clicked(object sender, EventArgs e)
		{
			if (sender is MatProcSelectorView)
			{
				MatProcSelectorView mpsView = (MatProcSelectorView) sender;

				if (mpsView.BindingContext is MatProcVM)
				{
					MatProcVM mpVM = (MatProcVM)mpsView.BindingContext;
					App.tdVM.Material = mpVM.Material;
					App.tdVM.Process = mpVM.GMAWSelected ? GMAW : (mpVM.GTAWSelected ? GTAW : null);
				}
			}
		}

		protected void OnSelectionChanged(object sender, StateEventArgs args)
		{
			MatProcVM selectedMatProcVM = (sender as MatProcVM);

			if (sender != this.aluMatProcMV && args.State && this.aluMatProcMV.MaterialSelected)
				this.aluMatProcMV.Unselect();

			if (sender != this.mildSteelMatProcMV && args.State && this.mildSteelMatProcMV.MaterialSelected)
				this.mildSteelMatProcMV.Unselect();

			if (sender != this.stainlessSteelMatProcMV && args.State && this.stainlessSteelMatProcMV.MaterialSelected)
				this.stainlessSteelMatProcMV.Unselect();

			if (selectedMatProcVM.MaterialSelected != args.State && args.PropertyName == "MaterialSelected")
			{
				if (args.State)
					selectedMatProcVM.MaterialSelected = true;
				else
				{
					App.tdVM.Material = null;
					App.tdVM.Process = null;
					App.tdVM.CorgonMison = 0;
					selectedMatProcVM.Unselect();
				}
			}

			if (args.PropertyName == "GMAWSelected")
			{
				if (args.State)
				{
					selectedMatProcVM.MaterialSelected = true;

					if (selectedMatProcVM.GTAWSelected)
						selectedMatProcVM.GTAWSelected = false;

					if (selectedMatProcVM.HasCorgon && !selectedMatProcVM.MisonSelected)
						selectedMatProcVM.CorgonSelected = true;
					else
					{
						selectedMatProcVM.CorgonSelected = false;
						selectedMatProcVM.MisonSelected = false;
					}

					App.tdVM.Process = GMAW;

					if (App.TutState < 2)
						Device.BeginInvokeOnMainThread(async () => await this.PerformTut2());

				}
				else
				{
					App.tdVM.Process = null;
					App.tdVM.CorgonMison = 0;
					selectedMatProcVM.MisonSelected = false;
					selectedMatProcVM.CorgonSelected = false;
				}
			}

			if (args.PropertyName == "GTAWSelected")
			{
				if (args.State)
				{
					selectedMatProcVM.MaterialSelected = true;

					if (selectedMatProcVM.GMAWSelected)
						selectedMatProcVM.GMAWSelected = false;

					if (selectedMatProcVM.CorgonSelected)
						selectedMatProcVM.CorgonSelected = false;

					if (selectedMatProcVM.MisonSelected)
						selectedMatProcVM.MisonSelected = false;

					App.tdVM.Process = GTAW;
					App.tdVM.CorgonMison = 0;
					selectedMatProcVM.CorgonSelected = false;
					selectedMatProcVM.MisonSelected = false;

					if (App.TutState < 2)
						Device.BeginInvokeOnMainThread(async () => await this.PerformTut2());
				}
				else
					App.tdVM.Process = null;
			}

			if (args.PropertyName == "CorgonSelected")
			{
				if (args.State)
				{
					selectedMatProcVM.MaterialSelected = true;

					if (selectedMatProcVM.GTAWSelected)
						selectedMatProcVM.GTAWSelected = false;

					if (selectedMatProcVM.MisonSelected)
						selectedMatProcVM.MisonSelected = false;

					selectedMatProcVM.GMAWSelected = true;
					//if (selectedMatProcVM.GMAWSelected)
					//	selectedMatProcVM.GMAWSelected = false;

					App.tdVM.Process = GMAW;
					App.tdVM.CorgonMison = CorgonMison.Corgon;
				}
				//else
					//App.tdVM.Process = null;
			}

			if (args.PropertyName == "MisonSelected")
			{
				if (args.State)
				{
					selectedMatProcVM.MaterialSelected = true;

					if (selectedMatProcVM.GTAWSelected)
						selectedMatProcVM.GTAWSelected = false;

					if (selectedMatProcVM.CorgonSelected)
						selectedMatProcVM.CorgonSelected = false;

					selectedMatProcVM.GMAWSelected = true;
					//if (selectedMatProcVM.GMAWSelected)
					//	selectedMatProcVM.GMAWSelected = false;

					App.tdVM.Process = GMAW;
					App.tdVM.CorgonMison = CorgonMison.Mison;
					selectedMatProcVM.MisonSelected = true;

					App.tdVM.PreselectIfNot();
				}
				//else
				//App.tdVM.Process = null;
			}
		}

		/*protected override void OnBindingContextChanged()
		{
			base.OnBindingContextChanged();

			if (BindingContext is TechnicalDataVM)
			{
				TechnicalDataVM tdVM = (TechnicalDataVM)BindingContext;

				this.thicknessRange0.IsVisible = tdVM.TechnicalData[0] != null && tdVM.TechnicalData[0].ThicknessRangeTo > 0;
				this.thicknessRange1.IsVisible = tdVM.TechnicalData[1] != null && tdVM.TechnicalData[1].ThicknessRangeTo > 0;
				this.thicknessRange2.IsVisible = tdVM.TechnicalData[2] != null && tdVM.TechnicalData[2].ThicknessRangeTo > 0;
				this.thicknessRange3.IsVisible = tdVM.TechnicalData[3] != null && tdVM.TechnicalData[3].ThicknessRangeTo > 0;
				this.thicknessRange4.IsVisible = tdVM.TechnicalData[4] != null && tdVM.TechnicalData[4].ThicknessRangeTo > 0;
				this.thicknessRange5.IsVisible = tdVM.TechnicalData[5] != null && tdVM.TechnicalData[5].ThicknessRangeTo > 0;
			}
			else
			{
				this.thicknessRange0.IsVisible = false;
				this.thicknessRange1.IsVisible = false;
				this.thicknessRange2.IsVisible = false;
				this.thicknessRange3.IsVisible = false;
				this.thicknessRange4.IsVisible = false;
				this.thicknessRange5.IsVisible = false;
			}
		}*/

		protected void OnEmissionPressed(object sender, EventArgs args)
		{
			Label box = (Label)sender;
			box.BackgroundColor = Color.Gray;

			TappedEventArgs tapArgs = (TappedEventArgs) args;

			App.tdVM.Emission = (string) tapArgs.Parameter;

			this.o3Label.BackgroundColor = App.tdVM.Emission != "O3" ? Color.Transparent : Color.Gray;
			this.noLabel.BackgroundColor = App.tdVM.Emission != "NO" ? Color.Transparent : Color.Gray;
			this.no2Label.BackgroundColor = App.tdVM.Emission != "NO2" ? Color.Transparent : Color.Gray;
			this.coLabel.BackgroundColor = App.tdVM.Emission != "CO" ? Color.Transparent : Color.Gray;
		}

		protected virtual void OnOverviewTapped(object sender, EventArgs args)
		{
			Navigation.PushAsync(new OverviewPage());
		}

		public void OnOptionsPressed(object sender, EventArgs args)
		{
			//Navigation.PushAsync(new OverviewPage());
			App.tdVM.Options = !App.tdVM.Options;
		}

		protected async override void OnSizeAllocated(double width, double height)
		{
			base.OnSizeAllocated(width, height);

			await Task.Delay(100);

			Page currentPage = Navigation.NavigationStack.Last();
			Page prevPage = Navigation.NavigationStack[Navigation.NavigationStack.Count-1];

			if(!(currentPage.GetType() == typeof(ComparePage)))
				if (height > width)
				{
					if (App.tdVM.Material != null && App.tdVM.Process != null)
					{
						if (prevPage is ComparePage)
							await Navigation.PopAsync();
						else
							await Navigation.PushAsync(new ComparePage(), false);

					if (App.TutState >= 1)
							this.tut02Image.IsVisible = false;

					}
				}
				else
				{
				}
		}

		protected async void OnTutImageTapped(object sender, EventArgs args)
		{
			Image image = (Image)sender;
			await image.FadeTo(0);
			image.IsVisible = false;

			App.TutState = 1;
			//image.RotateTo(
		}

		protected async Task PerformTut2()
		{
			if(App.TutState == 0)
				this.OnTutImageTapped(this.tut01Image, null);
			
			await Task.Delay(1250);

			this.tut02Image.IsVisible = true;
			this.tut02Image.FadeTo(1, 500, Easing.SinIn);
			await this.tut02Image.RotateTo(360, 750);
		}

		protected async void OnTut2ImageTapped(object sender, EventArgs args)
		{
			Image image = (Image)sender;
			await image.FadeTo(0);
			image.IsVisible = false;

			App.TutState = 2;
		}
	}
}
