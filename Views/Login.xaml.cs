﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace LindeGas3
{
	public partial class Login : ContentPage
	{
		string number = "";

		public Login()
		{
			InitializeComponent();
			NavigationPage.SetHasNavigationBar(this, false);
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			this.activityIndicator.IsRunning = false;
		}

		public async void OnEnterPressed(object sender, EventArgs args)
		{
			if (this.number.Length > 4)
			{
				this.activityIndicator.IsRunning = true;
				await Navigation.PushAsync(new LindeGasPage());

			}
		}

		public async void OnNumber1Pressed(object sender, EventArgs args)
		{
			Label box = (Label) sender;
			box.BackgroundColor = Color.Gray;

			await Task.Delay(100);

			box.BackgroundColor = Color.Transparent;

			TappedEventArgs tapArgs = (TappedEventArgs)args;

			System.Diagnostics.Debug.WriteLine(tapArgs.Parameter);

			number += tapArgs.Parameter;

			this.entry1.IsVisible = this.number.Length > 0;
			this.entry2.IsVisible = this.number.Length > 1;
			this.entry3.IsVisible = this.number.Length > 2;
			this.entry4.IsVisible = this.number.Length > 3;
			this.entry5.IsVisible = this.number.Length > 4;

			if (this.number.Length > 4)
			{
				this.enterPin.TextColor = Color.FromRgb(0, 255, 0);
				this.enterPin.Text = "ENTER";
			}
		}
	}
}
