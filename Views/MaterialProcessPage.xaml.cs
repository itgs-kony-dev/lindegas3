using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace LindeGas3
{
	public partial class MaterialProcessPage : ContentPage
	{
		IEnumerable<ICombination> combos;

		Metal selectedMetal;

		Process selectedProcess;

		bool dontSelect;

		public MaterialProcessPage()
		{
			InitializeComponent();

			combos = App.TechnicalData;
			//combos = App.RootObject.Emissions;
			//combos = combos.Union(App.RootObject.TechnicalData);
			combos = combos.Union(App.CrossSections);
			//combos = combos.Union(App.RootObject.WikiData);

			IEnumerable<Metal> currentGases = combos
				.GroupBy(x => x.Metal)
				.Select(x => x.First())
				.Select(x => x.Metal)
				.Join(
				App.Metals,
					combo => combo,
					metal => metal.Name,
					(combo, metal) => metal
				)
				.OrderBy(x => x.Name)
				.ToList();

			IEnumerable<Process> currentProcesses = combos
				.GroupBy(x => x.Process)
				.Select(x => x.First())
				.Select(x => x.Process)
				.Join(
					App.Processes,
					combo => combo,
					process => process.Name,
					(combo, process) => process
				)
				.OrderBy(x => x.Name)
				.ToList();

			this.materials.ItemsSource = currentGases;
			this.process.ItemsSource = currentProcesses;
		}

		public void Continue(object sender, EventArgs args)
		{
			LindeGas3Page lg2p = new LindeGas3Page();
		
			Navigation.PushAsync(lg2p);

		}

		public void OnMaterialSelected(object sender, SelectedItemChangedEventArgs args)
		{
			if (args.SelectedItem is Metal)
			{
					this.selectedMetal = (Metal)args.SelectedItem;
			}

			CheckSelectStates();

			dontSelect = true;
			//System.Diagnostics.Debug.WriteLine(args);

			IEnumerable<Process> currentProcesses = combos?
				.Where(x => x.Metal == this.selectedMetal?.Name)
				.GroupBy(x => x.Process)
				.Select(x => x.First())
				.Select(x => x.Process)
				.Join(
					App.Processes,
					combo => combo,
					process => process.Name,
					(combo, process) => process
				)
				.OrderBy(x => x.Name)
				.ToList();



			this.process.ItemsSource = currentProcesses;

			this.process.IsVisible = true;

		}

		public void OnMaterialTapped(object sender, ItemTappedEventArgs args)
		{
			if (args.Item is Metal && !dontSelect)
			{
				if (this.selectedMetal == args.Item)
				{
					this.selectedMetal = null;

					Device.BeginInvokeOnMainThread(async () =>
					{
						await Task.Delay(1000).ContinueWith((args2) => {
							materials.SelectedItem = null;
						});
						//materials.SelectedItem = null;
					});
				}
			}

			dontSelect = false;
		}

		public void OnProcessSelected(object sender, SelectedItemChangedEventArgs args)
		{
			if (args.SelectedItem is Process)
			{
				this.selectedProcess = (Process) args.SelectedItem;
			}

			CheckSelectStates();


			//System.Diagnostics.Debug.WriteLine(args);
		}

		protected void CheckSelectStates()
		{
			if (this.selectedProcess != null && this.selectedMetal != null)
			{
				this.continueView.IsVisible = true;
			}
			else
				this.continueView.IsVisible = false;
		}

	}
}
