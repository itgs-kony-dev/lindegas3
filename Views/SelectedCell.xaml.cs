using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace LindeGas3
{
	public partial class SelectedCell : ViewCell
	{
		public SelectedCell()
		{
			InitializeComponent();

			this.Tapped += (object sender, EventArgs e) =>
			{
				this.View.BackgroundColor = Color.Red;
				//OnListViewTextCellTapped(cell);            //Run your actual `Tapped` event code
				this.View.BackgroundColor = Color.Green; //Turn it back to the default color after your event code is done

			};
		}

	}
}
