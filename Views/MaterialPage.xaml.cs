using System;
using System.Collections.Generic;
using System.Linq;

using Xamarin.Forms;

using Urho;
using Urho.Forms;

using FormsSample;

namespace LindeGas3
{
	public partial class MaterialPage : ContentPage
	{
		IEnumerable<Metal> metals;

		public MaterialPage()
		{
			InitializeComponent();

			Title = "Material";

			//urho = new UrhoSurface { HorizontalOptions = LayoutOptions.FillAndExpand, VerticalOptions = LayoutOptions.FillAndExpand };
			//Test.Content = urho;


			this.metals = App.Metals;
		}

		public void OnAluminiumSelected(object sender, EventArgs args)
		{
			Navigation.PushAsync(new ProcessPage() { Material = this.metals.FirstOrDefault(x => x.Name == "Aluminium") });
		}

		public void OnStainlessSteelSelected(object sender, EventArgs args)
		{
			Navigation.PushAsync(new ProcessPage() { Material = this.metals.FirstOrDefault(x => x.Name == "Stainless steel") });
		}

		public void OnMildSteelSelected(object sender, EventArgs args)
		{
			Navigation.PushAsync(new ProcessPage() { Material = this.metals.FirstOrDefault(x => x.Name == "Mild steel") });
		}

		protected override async void OnAppearing()
		{
			base.OnAppearing();

			//charts = await urho.Show<Charts>(new ApplicationOptions(assetsFolder: null) { Orientation = ApplicationOptions.OrientationType.LandscapeAndPortrait });

			bool isAcknowledged = DependencyService.Get<IUserSettings>().RestoreBool("IsAcknowleded");

			if (!isAcknowledged)
			{
				var answer = await DisplayAlert("CONFIDENTIALITY NOTICE", "This App contains information property of Linde Group that is confidential, privileged and/or exempt from disclosure under applicable law.", "Accept", "Decline");

				if (answer == false)
					throw new Exception();
				else
					DependencyService.Get<IUserSettings>().Save("IsAcknowleded", true);
			}
		}
	}
}
