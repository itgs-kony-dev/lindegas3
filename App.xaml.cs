
using System;
using System.Reflection;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Xamarin;
using Xamarin.Forms;

using Newtonsoft.Json;

using Microsoft.WindowsAzure.MobileServices;
using Microsoft.WindowsAzure.MobileServices.SQLiteStore;

using Nito.AsyncEx;
using Microsoft.WindowsAzure.MobileServices.Files;
using Microsoft.WindowsAzure.MobileServices.Files.Metadata;
using Microsoft.WindowsAzure.MobileServices.Sync;

//using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace LindeGas3
{
	public partial class App : Xamarin.Forms.Application
	{
		public static RootObject RootObject { get; private set; }

		public static TechnicalDataVM tdVM { get; private set; }

		public static int TutState { get; set; } = 0;

		public static MobileServiceClient MobileService = new MobileServiceClient("https://lindewelding.azurewebsites.net");
		public static MobileServiceSQLiteStoreExt Store { get; private set; }

		public static CloudStorageAccount storageAccount = CloudStorageAccount.Parse(@"DefaultEndpointsProtocol=https;AccountName=lindewelding;AccountKey=D9UP2JuslA6RuszLhIag89jIXpnjVyVs8uYZoUvcCTnZpXJVWpGmy+WISOx8FntRb72bNZctB2P4Lz4BBUhQuA==;BlobEndpoint=https://lindewelding.blob.core.windows.net/;TableEndpoint=https://lindewelding.table.core.windows.net/;QueueEndpoint=https://lindewelding.queue.core.windows.net/;FileEndpoint=https://lindewelding.file.core.windows.net/");

		public static readonly object Locker = new object();
		public static readonly SemaphoreSlim SemaphoreSlim = new SemaphoreSlim(1);
		public static readonly AsyncLock Mutex = new AsyncLock();

		public static Metal[] Metals;
		public static Emission[] Emissions;
		public static Gas[] Gases;
		public static Process[] Processes;
		public static WikiData[] WikiData;
		public static TechnicalData[] TechnicalData;
		public static CrossSection[] CrossSections;
		public static Model3DScan[] Model3DScan;

		public App()
		{
			InitializeComponent();

			//if (Client == null)
			//	Client = new MobileServiceClient(Constants.ApplicationURL);

			if (Store == null)
			{
				Store = new MobileServiceSQLiteStoreExt("localstore.db", App.Mutex);

				//Store.DefineTable<TodoItem>();
				Store.DefineTable<Metal>();
				//MobileService.InitializeFileSyncContext(new ImagesFileSyncHandler<Metal>(MobileService.GetSyncTable<Metal>()), Store);
				Store.DefineTable<Emission>();
				//MobileService.InitializeFileSyncContext(new ImagesFileSyncHandler<Emission>(MobileService.GetSyncTable<Emission>()), Store);
				Store.DefineTable<Process>();
				//MobileService.InitializeFileSyncContext(new ImagesFileSyncHandler<Process>(MobileService.GetSyncTable<Process>()), Store);
				Store.DefineTable<Gas>();
				//MobileService.InitializeFileSyncContext(new ImagesFileSyncHandler<Gas>(MobileService.GetSyncTable<Gas>()), Store);
				Store.DefineTable<WikiData>();
				//MobileService.InitializeFileSyncContext(new ImagesFileSyncHandler<WikiData>(MobileService.GetSyncTable<WikiData>()), Store);
				Store.DefineTable<TechnicalData>();
				//MobileService.InitializeFileSyncContext(new ImagesFileSyncHandler<TechnicalData>(MobileService.GetSyncTable<TechnicalData>()), Store);
				Store.DefineTable<CrossSection>();
				//MobileService.InitializeFileSyncContext(new ImagesFileSyncHandler<CrossSection>(MobileService.GetSyncTable<CrossSection>()), Store);
				Store.DefineTable<Model3DScan>();
				//MobileService.InitializeFileSyncContext(new ImagesFileSyncHandler<Model3DScan>(MobileService.GetSyncTable<Model3DScan>()), Store);

				MobileService.InitializeFileSyncContext(new ImagesFileSyncHandler<CrossSection>(MobileService.GetSyncTable<CrossSection>()), Store);

				//MobileService.SyncContext.InitializeAsync(Store, StoreTrackingOptions.AllNotifications);

				//App.MobileService.InitializeFileSyncContext(new ImagesFileSyncHandler<CrossSection>(table), Store);
				App.MobileService.SyncContext.InitializeAsync(Store /*StoreTrackingOptions.AllNotifications*/);
			}

			//Initializes the SyncContext using the default IMobileServiceSyncHandler.

			var assembly = typeof(App).GetTypeInfo().Assembly;
			Stream stream = assembly.GetManifestResourceStream("Emissions.json");

			using (var reader = new StreamReader(stream))
			{
				var json = reader.ReadToEnd();
				RootObject = JsonConvert.DeserializeObject<RootObject>(json);
			}

			MainPage = new Page();
		}

		protected async override void OnStart()
		{
			var metals = await ItemManager<Metal>.Instance.GetItemsAsync(true);
			Metals = metals.ToArray();

			var emissions = await ItemManager<Emission>.Instance.GetItemsAsync(true);
			Emissions = emissions.ToArray();

			var gases = await ItemManager<Gas>.Instance.GetItemsAsync(true);
			Gases = gases.ToArray();

			var processes = await ItemManager<Process>.Instance.GetItemsAsync(true);
			Processes = processes.ToArray();

			var wikiData = await ItemManager<WikiData>.Instance.GetItemsAsync(true);
			WikiData = wikiData.ToArray();

			var technicalData = await ItemManager<TechnicalData>.Instance.GetItemsAsync(true);
			TechnicalData = technicalData.ToArray();

			await ItemManager<CrossSection>.Instance.SyncAsync();
			var crossSections = await ItemManager<CrossSection>.Instance.GetItemsAsync(true);
			CrossSections = crossSections.ToArray();

			var model3DScans = await ItemManager<Model3DScan>.Instance.GetItemsAsync(true);
			Model3DScan = model3DScans.ToArray();

			tdVM = new TechnicalDataVM();

			NavigationPage navPage = new NavigationPage(new LindeGasPage());

			MainPage = navPage;

			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
	}
}

