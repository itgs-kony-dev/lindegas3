﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace App2Heaven
{
	public interface IPopupInterface
	{
		void ShowPopup();
		void ShowPopup(View view);
		Task<int> Show(string title, string cancel, string destruction, List<Tuple<string, int, string>> entries);
		Task<string> ShowPasswordDialog();
		Task<string> ShowPasswordCheck();
	}
}

