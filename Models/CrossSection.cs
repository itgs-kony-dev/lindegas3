using System;

using Xamarin.Forms;

using System.ComponentModel;

using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json;

namespace LindeGas3
{
	public class CrossSection : INotifyPropertyChanged, ICombination, IIdentifiable
	{
		[JsonProperty("id")]
		public string Id { get; set; }

		public string Metal { get; set; }
		public string Process { get; set; }
		public string Gas { get; set; }
		public string Filename { get; set; }

		public string Type { get; set; }

		string filename2;

		[JsonIgnore]
		public ImageSource ImageSource
		{
			get { return ImageSource.FromResource(Filename); }
		}

		[JsonIgnore]
		public ImageSource ImageSource2
		{
			get { return ImageSource.FromResource(Filename2); }
		}

		[JsonIgnore]
		public string Filename2
		{
			set
			{
				this.filename2 = value;
				OnPropertyChanged("ImageSource2");
			}
			get { return this.filename2; }
		}

		public event PropertyChangedEventHandler PropertyChanged;

		protected void OnPropertyChanged(string propertyName)
		{
			try
			{
				PropertyChangedEventHandler handler = PropertyChanged;

				if (handler != null)
					handler(this, new PropertyChangedEventArgs(propertyName));
			}
			catch (Exception exception)
			{
				System.Diagnostics.Debug.WriteLine("Exception: " + exception.Message);
			}
		}

		[Version]
		public string Version { get; set; }
	}
}

