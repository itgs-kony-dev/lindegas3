using System;

using Newtonsoft.Json;

namespace LindeGas3
{
	public class Emission : ICombination, IIdentifiable
	{
		[JsonProperty("ID")]
		public string Id { get; set; }

		[JsonProperty("Metal")]
		public string Metal { get; set; }

		[JsonProperty("Process")]
		public string Process { get; set; }

		[JsonProperty("Gas")]
		public string Gas { get; set; }

		[JsonProperty("Speed")]
		public string Speed { get; set; }

		[JsonProperty("Speed_Unit")]
		public string Speed_Unit { get; set; }

		[JsonProperty("min")]
		public double min { get; set; }

		[JsonProperty("max")]
		public double max { get; set; }

		[JsonProperty("_10A")]
		public double _10A { get; set; }

		[JsonProperty("MaxScale")]
		public double MaxScale { get; set; }

		[JsonProperty("Emission_type")]
		public string Emission_type { get; set; }

		[JsonProperty("emission")]
		public double emission { get; set; }

		[JsonProperty("Emission_Unit")]
		public string Emission_Unit { get; set; }
	}
}

