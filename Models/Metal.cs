using System;

using Newtonsoft.Json;

namespace LindeGas3
{
	public class Metal : IIdentifiable
	{
		[JsonProperty("Name")]
		public string Name { get; set; }

		[JsonProperty("ID")]
		public string Id { get; set; }

		public override string ToString()
		{
			return Name.ToString();
		}
	}
}

