using System;

using Xamarin.Forms;

using Newtonsoft.Json;

namespace LindeGas3
{
	public class TechnicalData : ICombination, IIdentifiable
	{
		[JsonProperty("ID")]
		public string Id { get; set; }

		public string Metal { get; set; }
		public string Process { get; set; }
		public string Gas { get; set; }
		public int WeldingSpeed { get; set; }
		public int PorosityControl { get; set; }
		public int Fusion { get; set; }
		public int Penetration { get; set; }
		public int EaseOfUse { get; set; }
		public double ThicknessRangeFrom { get; set; }
		public double ThicknessRangeTo { get; set; } = 0;
		public string Description { get; set; }

	}
}

