﻿using System;

using Newtonsoft.Json;

namespace LindeGas3
{
	public class TodoItem : IIdentifiable
	{
		public TodoItem()
		{
		}

		[JsonProperty("ID")]
		public string Id { get; set; }
		public string Text { get; set; }

	}
}
