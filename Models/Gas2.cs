using System;
using System.Collections.Generic;
using System.ComponentModel;

using Xamarin.Forms;

using Newtonsoft.Json;

namespace LindeGas3
{
	public class Gas2 : INotifyPropertyChanged, IIdentifiable
	{
		[JsonProperty("ID")]
		public string ID { get; set; }

		[JsonProperty("Name")]
		public string Name { get; set; }
		public UnselectedCell Cell { get; set; }

		[JsonProperty("IsPremium")]
		public bool IsPremium;
		//public IEnumerable<Gas> Parent { get; set; }
		public string SelectedList { get; set; }

		public Gas2 Brother { get; set; }

		bool isSelected;
		public Color backgroundColor = Color.FromHex("#367a99");

		public event PropertyChangedEventHandler PropertyChanged;

		protected void OnPropertyChanged(string propertyName)
		{
			try
			{
				PropertyChangedEventHandler handler = PropertyChanged;

				if (handler != null)
					handler(this, new PropertyChangedEventArgs(propertyName));
			}
			catch (Exception exception)
			{
				System.Diagnostics.Debug.WriteLine("Exception: " + exception.Message);
			}
		}

		public override string ToString()
		{
			return Name;
		}

		public event OnSelectionChangedHandler OnSelected;

		public bool IsSelected
		{
			set
			{
				this.isSelected = value;
				OnPropertyChanged("IsSelected");

				if (OnSelected != null)
					OnSelected(this, new StateEventArgs(value, "IsSelected"));

				if (this.IsSelected)
				{
					if (Brother != null)
					{
						this.Brother.IsSelected = false;
						this.Brother.BackgroundColor = Color.Gray;
					}

					this.BackgroundColor = Color.FromHex("#aadce8");
				}
				else
				{
					if (Brother != null)
					{
						this.Brother.BackgroundColor = Color.FromHex("#367a99");
					}
					else
					{

					}
					this.BackgroundColor = Color.FromHex("#367a99");
				}
			}
			get { return this.isSelected; }
		}

		Color BackgroundColor
		{
			set
			{
				this.backgroundColor = value;
				OnPropertyChanged("BackgroundColor");
			}
			get { return this.backgroundColor; }
		}

		public Gas2()
		{

		}
	}
}

