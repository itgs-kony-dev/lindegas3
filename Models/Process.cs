using System;

using Newtonsoft.Json;

namespace LindeGas3
{
	public class Process : IIdentifiable
	{
		[JsonProperty("ID")]
		public string Id { get; set; }

		[JsonProperty("Name")]
		public string Name { get; set; }

		public override string ToString()
		{
			return Name.ToString();
		}
	}
}

