using System;

using Xamarin.Forms;

using Newtonsoft.Json;

namespace LindeGas3
{
	public class Model3DScan : ModelBase, ICombination, IIdentifiable
	{
		[JsonProperty("ID")]
		public string Id { get; set; }
		public string Metal { get; set; }
		public string Process { get; set; }
		public string Gas { get; set; }
		public string Filename { get; set; }
		public string Material { get; set; }
		public string Preview { get; set; }


	}
}

