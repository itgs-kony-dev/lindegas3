using System;

using Xamarin.Forms;

using Newtonsoft.Json;
using System.ComponentModel;

namespace LindeGas3
{
	public class WikiData : INotifyPropertyChanged, ICombination, IIdentifiable
	{
		[JsonProperty("ID")]
		public string Id { get; set; }

		[JsonProperty("Metal")]
		public string Metal { get; set; }

		[JsonProperty("Process")]
		public string Process { get; set; }

		[JsonProperty("Gas")]
		public string Gas { get; set; }

		[JsonProperty("Filename")]
		public string Filename { get; set; }

		string filename { get; set; }
		string filename2 { get; set; }

		[JsonIgnore]
		public string Filename2
		{
			set
			{
				this.filename2 = value;
				OnPropertyChanged("Filename2");
			}
			get { return this.filename2; }
		}

		public event PropertyChangedEventHandler PropertyChanged;

		protected void OnPropertyChanged(string propertyName)
		{
			try
			{
				PropertyChangedEventHandler handler = PropertyChanged;

				if (handler != null)
					handler(this, new PropertyChangedEventArgs(propertyName));
			}
			catch (Exception exception)
			{
				System.Diagnostics.Debug.WriteLine("Exception: " + exception.Message);
			}
		}
	}
}

