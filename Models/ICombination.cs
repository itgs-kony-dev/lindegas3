using System;
namespace LindeGas3
{
	public interface ICombination
	{
		string Metal { get; set; }
		string Process { get; set; }
		string Gas { get; set; }
	}
}

