using System;

namespace LindeGas3
{
	public interface IUserSettings
	{
		void Save(string key, object value);
		bool RestoreBool(string key);
	}
}
