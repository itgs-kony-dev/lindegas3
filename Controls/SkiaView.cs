using System;
using System.Collections.Generic;
using System.Linq;

using Xamarin.Forms;

using SkiaSharp;

namespace LindeGas3
{
	public interface ISkiaViewController : IViewController
	{
		void SendDraw(SKCanvas canvas);
		void SendTap();
	}

	public class SkiaView : View, ISkiaViewController
	{

		protected Emission[][] emissions;
		protected SKCanvas canvas;

		public SkiaView()
		{
			//this.sample = sample;
		}

		void ISkiaViewController.SendDraw(SKCanvas canvas)
		{
			Draw(canvas);
		}

		void ISkiaViewController.SendTap()
		{
			//sample?.TapMethod?.Invoke();
		}

		protected virtual void Draw(SKCanvas canvas)
		{
			try
			{
				if (emissions == null)
					return;

				this.canvas = canvas;

				canvas.Clear(new SKColor(0xE5, 0xED, 0xEF));

				SKColor[] colors = new SKColor[] {
				new SKColor(0x00, 0x00, 0x80),
				//new SKColor(0xff, 0x00, 0x00),
				new SKColor(0x00, 0xe0, 0x00),
				new SKColor(0x00, 0xef, 0xef),
				new SKColor(0xef, 0xef, 0x00)
			};

				List<Emission> emissionList = new List<Emission>(emissions[0]);//.Select((arg1) => );

				Emission[] sortedEmissions = new Emission[4];
				sortedEmissions[0] = emissionList.FirstOrDefault(x => x.Emission_type.ToUpper() == "CO");
				//sortedEmissions[1] = emissionList.FirstOrDefault(x => x.Emission_type.ToUpper().Contains("CO2"));
				sortedEmissions[1] = emissionList.FirstOrDefault(x => x.Emission_type.ToUpper() == "NO");
				sortedEmissions[2] = emissionList.FirstOrDefault(x => x.Emission_type.ToUpper().Contains("NO2"));
				sortedEmissions[3] = emissionList.FirstOrDefault(x => x.Emission_type.ToUpper().Contains("O3"));
				Emission ffr = emissionList.FirstOrDefault(x => x.Emission_type.ToUpper().Contains("FFR"));
				Emission ffr2 = null;

				List<Emission> emissionList2 = null;

				Emission[] sortedEmissions2 = new Emission[4];

				if (emissions.Length > 1)
				{
					emissionList2 = new List<Emission>(emissions[1]);

					sortedEmissions2[0] = emissionList2.FirstOrDefault(x => x.Emission_type.ToUpper() == "CO");
					//sortedEmissions2[1] = emissionList2.FirstOrDefault(x => x.Emission_type.ToUpper().Contains("CO2"));
					sortedEmissions2[1] = emissionList2.FirstOrDefault(x => x.Emission_type.ToUpper() == "NO");
					sortedEmissions2[2] = emissionList2.FirstOrDefault(x => x.Emission_type.ToUpper().Contains("NO2"));
					sortedEmissions2[3] = emissionList2.FirstOrDefault(x => x.Emission_type.ToUpper().Contains("O3"));
					ffr2 = emissionList2.FirstOrDefault(x => x.Emission_type.ToUpper().Contains("FFR"));

				}

				SKPaint fontPaint = new SKPaint();
				fontPaint.Color = SKColors.Black;
				fontPaint.TextSize = 24.0f;
				fontPaint.IsAntialias = true;
				fontPaint.IsStroke = false;

				SKPaint sFontPaint = new SKPaint();
				sFontPaint.Color = SKColors.Black;
				sFontPaint.TextSize = 17.0f;
				sFontPaint.IsAntialias = true;
				//sFontPaint.IsStroke = false;
				//fontPaint.MaskFilter = SKMaskFilter.CreateEmboss(0.5f, 1.0f, 1.0f, 1.0f, 0.1f, 0.1f);

				SKPaint tFontPaint = new SKPaint();
				tFontPaint.Color = SKColors.Black;
				tFontPaint.TextSize = 10.0f;
				tFontPaint.IsAntialias = true;

				SKPaint linePaint = new SKPaint();
				linePaint.Color = SKColors.Gray;
				/*SKMatrix lattice = new SKMatrix();
				lattice.ScaleX = 80.0f;
				lattice.ScaleY = 80.0f;

				//lattice.preRotate(30.0f);

				try
				{
					linePaint.PathEffect = SKPathEffect.Create2DLine(1.0f, lattice);
				}
				catch (Exception exception)
				{
					System.Diagnostics.Debug.WriteLine("Exception: " + exception.Message);
				}

				linePaint.IsAntialias = true;
				/*SKRect bounds = new SKRect(100, 100, 500, 400);
				//(void)canvas->getClipBounds(&bounds);
				//bounds.(8.0f, 8.0f);
				//canvas->clear(SK_ColorWHITE);
				canvas.DrawRect(bounds, linePaint);*/


				/*for (float y = 100; y <= 380; y += 20)
				{
					canvas.DrawLine(20, y, 100f, y, linePaint);
					canvas.DrawLine(260, y, 720f, y, linePaint);
				}*/

				if (this.emissions == null || this.emissions.Count() == 0)
					return;

				double max = 0;

				if (sortedEmissions.Count() > 0)
				{
					max = sortedEmissions.Where(x => x != null && x.Emission_type.ToUpper() != "CO").Max(x => x.emission);

					if (emissions.Length > 1)
						max = sortedEmissions.Where(x => x != null && x.Emission_type.ToUpper() != "CO").Union(sortedEmissions2).Max(x => x.emission);

				}

				for (int i = 0; i < 4; i++)
				{
					//if (sortedEmissions[i] == null && sortedEmissions2[i] == null)
					//	continue;

					// set up drawing tools
					for (float y = 100; y <= 380; y += 20)
					{
						canvas.DrawLine(20 + i * 120, y, 100 + i * 120, y, linePaint);
						//canvas.DrawLine(260, y, 720f, y, linePaint);
					}

					using (var paint = new SKPaint())
					{
						paint.IsAntialias = true;
						paint.Color = colors[i];
						paint.TextSize = 24.0f;
						paint.StrokeCap = SKStrokeCap.Round;
						paint.MaskFilter = SKMaskFilter.CreateEmboss(2.0f, 1.0f, 1.0f, 1.0f, 3.0f, 1.5f);
						//paint.MaskFilter = SKMaskFilter.CreateGamma(50.5f);

						SKColor color2 = new SKColor((byte)(colors[i].Red * 0.5), (byte)(colors[i].Green * 0.5), (byte)(colors[i].Blue * 0.5));

						paint.Shader = SKShader.CreateLinearGradient(new SKPoint(100 + i * 100, 80), new SKPoint(400 + i * 100, 180), new SKColor[] { colors[i], color2 }, new float[] { 0.0f, 1.0f }, SKShaderTileMode.Clamp);
						/*paint setShader(SkGradientShader::MakeLinear(
						 points, colors, nullptr, 2,
						 SkShader::kClamp_TileMode, 0, nullptr));*/

						if (sortedEmissions[i] != null || i == 0)
						{
							double v;

							/*if (i == 0)
								v = 300;
							else*/
							v = sortedEmissions[i].emission / sortedEmissions[i].MaxScale * 300;

							canvas.DrawRect(new SKRect(40 + i * 120, (float)(380 - v), 60 + i * 120, 380), paint);
							canvas.DrawText(sortedEmissions[i].Emission_type, i * 120 + 50, 410, fontPaint);
							canvas.DrawText(sortedEmissions[i].emission.ToString(), i * 120 + 65, (float)(377 - v + (v % 20)), sFontPaint);
							canvas.DrawText(sortedEmissions[i].MaxScale.ToString(), i * 120 + 20, 90, sFontPaint);
						}

						if ((sortedEmissions2[i] != null || i == 0) && emissions.Count() > 1)
						{
							double v;

							if (i == 0)
								v = 300;
							else
								v = sortedEmissions2[i].emission / max * 300;

							canvas.DrawRect(new SKRect(100 + i * 120, (float)(380 - v), 120 + i * 120, 380), paint);
							//canvas.DrawText(sortedEmissions2[i].Emission_type, i * 100 + 90, 450, fontPaint);
							canvas.DrawText(sortedEmissions2[i].emission.ToString(), i * 120 + 125, (float)(377 - v + (v % 20)), sFontPaint);
						}

						//canvas.DrawPaint(paint);
					}
				}

				canvas.DrawText("Emission [mL/min]", 20, 40, sFontPaint);

				canvas.DrawText("Fume Formation Rate [mg/s]", 20, 440, sFontPaint);

				SKRect rect = new SKRect(20, 480, 450, 500);
				SKPaint paint2 = new SKPaint();
				paint2.StrokeWidth = 1;
				paint2.StrokeCap = SKStrokeCap.Square;
				paint2.MaskFilter = SKMaskFilter.CreateEmboss(2.0f, 1.0f, 1.0f, 1.0f, 3.0f, 1.5f);
				//paint2.MaskFilter = SKMaskFilter.CreateBlur(SKBlurStyle.Outer, 10.0f);
				paint2.Shader = SKShader.CreateLinearGradient(new SKPoint(20, 490), new SKPoint(450, 490), new SKColor[] { new SKColor(192, 192, 192), new SKColor(160, 160, 168), new SKColor(128, 128, 144) }, new float[] { 0.0f, 0.5f, 1.0f }, SKShaderTileMode.Clamp);
				//paint2.Shader = SKShader.CreateLinearGradient(new SKPoint(150, 490), new SKPoint(450, 490), new SKColor[] { new SKColor(0, 255, 0), new SKColor(255, 255, 0), new SKColor(255, 0, 0) }, new float[] { 0.0f, 0.5f, 1.0f }, SKShaderTileMode.Clamp);
				canvas.DrawRect(rect, paint2);

				if (ffr != null)
				{
					SKRect rect2 = new SKRect(15 + (float)(ffr.emission * 30), 455, 25 + (float)(ffr.emission * 30), 530);
					SKPaint paint3 = new SKPaint();
					paint3.StrokeWidth = 5;
					paint3.IsStroke = true;
					paint3.StrokeCap = SKStrokeCap.Round;
					paint3.MaskFilter = SKMaskFilter.CreateEmboss(2.0f, 1.0f, 1.0f, 1.0f, 3.0f, 1.5f);

					canvas.DrawText(ffr.emission.ToString(), 35 + (float)(ffr.emission * 30), 475, sFontPaint);

					for (int i = 0; i <= 14; i++)
						canvas.DrawText(i.ToString(), 15 + i * 30, 520, sFontPaint);

					canvas.DrawRect(rect2, paint3);

					if (ffr2 != null)
					{
						paint3.Color = new SKColor(0, 0, 255);
						sFontPaint.Color = new SKColor(0, 0, 255);

						rect2 = new SKRect(145 + (float)(ffr2.emission * 30), 455, 155 + (float)(ffr2.emission * 30), 530);

						canvas.DrawRect(rect2, paint3);

						canvas.DrawText(ffr2.emission.ToString(), 165 + (float)(ffr2.emission * 30), 475, sFontPaint);
					}
				}

				canvas.DrawText("Particulate emissions: Tests have been carried out according to DIN ISO 15011-1 (blind welds)", 20, 560, tFontPaint);
				canvas.DrawText("Gaseoues esmissons: Tests have been carried out according to DIN EN ISO 15011-3 for ozone and", 20, 576, tFontPaint);
				canvas.DrawText("DIN EN ISO 15011-2 for all other gaseous emissions (blind welds)", 20, 588, tFontPaint);

			}
			catch (Exception exception)
			{
				System.Diagnostics.Debug.WriteLine(exception.Message + "  " + exception.StackTrace);
			};
		}

		protected override void OnBindingContextChanged()
		{
			base.OnBindingContextChanged();

			if (BindingContext is Emission[][])
			{
				this.emissions = BindingContext as Emission[][];

				//if (this.canvas != null)
				//	Draw(canvas);

			}
		}
	}
}

