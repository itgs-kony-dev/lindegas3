using System;

using Xamarin.Forms;

namespace LindeGas3
{
	public class VideoView : View
	{
		public static readonly BindableProperty FileNameProperty = BindableProperty.Create("FileName", typeof(string), typeof(VideoView), "");

		public string FileName
		{
			get { return (string) GetValue(FileNameProperty); }
			set { SetValue(FileNameProperty, value); }
		}

		public VideoView()
		{
		}
	}
}

