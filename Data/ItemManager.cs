﻿// To add offline sync support: add the NuGet package WindowsAzure.MobileServices.SQLiteStore
// to all projects in the solution and uncomment the symbol definition OFFLINE_SYNC_ENABLED
// For Xamarin.iOS, also edit AppDelegate.cs and uncomment the call to SQLitePCL.CurrentPlatform.Init()
// For more information, see: http://go.microsoft.com/fwlink/?LinkId=620342 
#define OFFLINE_SYNC_ENABLED

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using Xamarin.Forms;
using Microsoft.WindowsAzure.MobileServices;
using Microsoft.WindowsAzure.MobileServices.Files;
using System.IO;

#if OFFLINE_SYNC_ENABLED
using Microsoft.WindowsAzure.MobileServices.SQLiteStore;
using Microsoft.WindowsAzure.MobileServices.Sync;
#endif

namespace LindeGas3
{
	public delegate void OnSyncAsyncEventHandler(object sender, EventArgs e);

	public partial class ItemManager<T> where T : IIdentifiable
	{
		public event OnSyncAsyncEventHandler OnSyncAsync;

		//protected static ItemManager<T> defaultInstance = new ItemManager<T>();

		private static ItemManager<T> instance;

		public static ItemManager<T> Instance
		{
			get
			{
				if (instance == null)
					instance = new ItemManager<T>();

				return instance;
			}
		}

		protected MobileServiceClient client;

		IFileHelper fileHelper;

#if OFFLINE_SYNC_ENABLED
		protected IMobileServiceSyncTable<T> table;
#else
        protected IMobileServiceTable<T> table;
#endif

		public IMobileServiceSyncTable<T> Table { get { return table; } }

		protected ItemManager()
		{
			this.client = App.MobileService;

			this.fileHelper = DependencyService.Get<IFileHelper>();

#if OFFLINE_SYNC_ENABLED
			this.table = client.GetSyncTable<T>();
#else
            this.stimulusTable = client.GetTable<T>();
#endif
		}

		public MobileServiceClient CurrentClient
		{
			get { return client; }
		}

		public bool IsOfflineEnabled
		{
			get { return table is Microsoft.WindowsAzure.MobileServices.Sync.IMobileServiceSyncTable<ItemManager<T>>; }
		}

		public virtual bool Writeable { get; set; }

		public virtual async Task<ObservableCollection<T>> GetItemsAsync(bool syncItems = false)
		{
			try
			{
#if OFFLINE_SYNC_ENABLED
				if (syncItems)
					await this.SyncAsync();
#endif
				IEnumerable<T> items = await table
					.ToEnumerableAsync();

				return new ObservableCollection<T>(items);
			}
			catch (MobileServiceInvalidOperationException msioe)
			{
				Debug.WriteLine(@"Invalid sync operation: {0}", msioe.Message);
			}
			catch (Exception e)
			{
				Debug.WriteLine(@"Sync error: {0}", e.Message);
			}
			return null;
		}

		public virtual async Task SaveItemAsync(T item, bool syncItems = false)
		{
			if ((item as IIdentifiable).Id == null)
			{
				if (Writeable)
				{
					await table.InsertAsync(item);
					/*var obj = table.InsertAsync (item);
					obj.Wait ();
					bool completed = obj.IsCompleted;
					var status = obj.Status;*/

				}
			}
			else
			{
				await table.UpdateAsync(item);
			}

#if OFFLINE_SYNC_ENABLED
			if (syncItems)
				await this.SyncAsync();
#endif
		}

		/*public override async Task SaveItemAsync<T>(T item)
        {
            await SaveItemAsync(item);
        }*/

#if OFFLINE_SYNC_ENABLED
		public virtual async Task<bool> SyncAsync()
		{
			ReadOnlyCollection<MobileServiceTableOperationError> syncErrors = null;

			await App.SemaphoreSlim.WaitAsync(15000);

			try
			{
				await this.table.MobileServiceClient.SyncContext.PushAsync();

				await this.table.PushFileChangesAsync();

				Debug.WriteLine("Start Sync: " + typeof(T).ToString());
				await this.table.PullAsync(
					"all" + typeof(T).ToString() + "s",
					this.table.CreateQuery());
				Debug.WriteLine("Finished Sync: " + typeof(T).ToString());
			}
			catch (MobileServicePushFailedException exc)
			{
				if (exc.PushResult != null)
				{
					syncErrors = exc.PushResult.Errors;
				}
			}
			catch (WebException exception)
			{
				System.Diagnostics.Debug.WriteLine("WebException:" + exception.Message);

				try
				{
					await client.SyncContext.PushAsync();
				}
				catch (Exception exception2)
				{
					System.Diagnostics.Debug.WriteLine("Push-Exception:" + exception2.Message);
				}
			}
			catch (Exception exception)
			{
				System.Diagnostics.Debug.WriteLine("Exception:" + exception.Message);
			}

			if (syncErrors != null)
			{
				foreach (var error in syncErrors)
				{
					if (error.OperationKind == MobileServiceTableOperationKind.Update && error.Result != null)
					{
						//Update failed, reverting to server's copy.
						await error.CancelAndUpdateItemAsync(error.Result);
					}
					else
					{
						// Discard local change.
						await error.CancelAndDiscardItemAsync();
					}

					Debug.WriteLine(@"Error executing sync operation. Item: {0} ({1}). Operation discarded.", error.TableName, error.Item["id"]);
				}
			}

			App.SemaphoreSlim.Release();

			OnSyncAsyncEvent();

			return true;
		}
#endif

		public virtual async Task DeleteItemAsync(T item)
		{
			try
			{
				if (Writeable)
				{
					await table.DeleteAsync(item);

#if OFFLINE_SYNC_ENABLED
					await SyncAsync();
#endif
				}
			}
			catch (Exception exception)
			{
				System.Diagnostics.Debug.WriteLine("Delete-Exception:" + exception.Message);
			}
		}

		internal async Task<MobileServiceFile> AddImage(T item, string imagePath)
		{
			
			string targetPath = fileHelper.CopyFileToAppDirectory(item.Id, imagePath);

			// FILES: Creating/Adding file
			MobileServiceFile file = null;

			try
			{
				file = await this.table.AddFileAsync(item, Path.GetFileName(targetPath));

				// "Touch" the record to mark it as updated
				await this.table.UpdateAsync(item);
			}
			catch (Exception exception)
			{
				Debug.WriteLine("Exception: " + exception);
			}

			return file;
		}

		protected void OnSyncAsyncEvent()
		{
			try
			{
				OnSyncAsync?.Invoke(this, new EventArgs());
			}
			catch (Exception exception)
			{
				System.Diagnostics.Debug.WriteLine("Exception: " + exception.Message);
			}
		}

		internal async Task<IEnumerable<MobileServiceFile>> GetImageFiles(T item)
		{
			// FILES: Get files (local)
			//if (requiresServerPull)
			//    await this.monkeyTable.PullFilesAsync(todoItem);
			return await this.table.GetFilesAsync(item);
		}
	}
}
