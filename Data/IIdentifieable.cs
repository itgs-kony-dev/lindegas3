﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LindeGas3
{
	public interface IIdentifiable
	{
		string Id { get; set; }
	}

	public interface IIdentifiable<T>
	{
		T ID { get; set; }
	}
}

