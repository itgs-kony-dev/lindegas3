﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nito.AsyncEx;
using Nito;
using Nito.AsyncEx.Synchronous;

using Microsoft.WindowsAzure.MobileServices;
using Microsoft.WindowsAzure.MobileServices.SQLiteStore;
using Microsoft.WindowsAzure.MobileServices.Sync;
using Newtonsoft.Json.Linq;

namespace LindeGas3
{
	public class MobileServiceSQLiteStoreExt : MobileServiceSQLiteStore
	{
		AsyncLock _mutex;

		public MobileServiceSQLiteStoreExt(string aDbPath, AsyncLock aMutex) : base(aDbPath)
		{
			_mutex = aMutex;
		}

		protected override void ExecuteNonQueryInternal(string sql, IDictionary<string, object> parameters)
		{  
			lock (App.Locker)
			{
				base.ExecuteNonQueryInternal(sql, parameters);
			}
		}

		protected override IList<JObject> ExecuteQueryInternal(TableDefinition table, string sql, IDictionary<string, object> parameters)
		{
			lock (App.Locker)
			{
				return base.ExecuteQueryInternal(table, sql, parameters);
			}
		}
	}
}
