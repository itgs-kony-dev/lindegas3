﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Microsoft.WindowsAzure.Storage.Blob;

namespace LindeGas3
{
	public interface IFileHelper
	{
		string FilePath { get; }
		string CopyFileToAppDirectory(string itemId, string filePath);
		string GetLocalFilePath(string itemId, string fileName);
		Task DownloadToFileAsync(CloudBlockBlob blob, string filePath);
		void DeleteLocalFile(Microsoft.WindowsAzure.MobileServices.Files.MobileServiceFile file);
	}
}