using System;

using Xamarin;
using Xamarin.Forms;

namespace LindeGas3
{
	class MyDataTemplateSelector : Xamarin.Forms.DataTemplateSelector
	{
		public MyDataTemplateSelector()
		{
			// Retain instances!
			this.incomingDataTemplate = new DataTemplate(typeof(UnselectedCell));
			this.outgoingDataTemplate = new DataTemplate(typeof(SelectedCell));
		}

		protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
		{
			Gas messageVm = item as Gas;

			if (messageVm == null)
				return null;

			return messageVm.IsSelected ? this.incomingDataTemplate : this.outgoingDataTemplate;
		}

		private readonly DataTemplate incomingDataTemplate;
		private readonly DataTemplate outgoingDataTemplate;
	}
}
