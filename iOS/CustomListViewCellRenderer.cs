using System;

using UIKit;

using Xamarin;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

using LindeGas3;

[assembly: ExportRenderer(typeof(ViewCell), typeof(LindeGas3.iOS.CustomListViewCellRenderer))]
namespace LindeGas3.iOS
{
	class CustomListViewCellRenderer : ViewCellRenderer
	{

		public override UITableViewCell GetCell(Cell item, UITableViewCell reusableCell, UITableView tv)
		{
			var cell = base.GetCell(item, reusableCell, tv);

			cell.SelectionStyle = UITableViewCellSelectionStyle.None;
			return cell;
		}
	}
}