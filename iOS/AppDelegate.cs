using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;


using CoreGraphics;
using Foundation;
using ObjCRuntime;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

using Microsoft.WindowsAzure.MobileServices;

namespace LindeGas3.iOS
{
	[Register("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{
			global::Xamarin.Forms.Forms.Init();

			CurrentPlatform.Init();

			LoadApplication(new App());

			var plist = NSUserDefaults.StandardUserDefaults;

			var isAcknowledged = plist.BoolForKey("IsAcknowledged");

			isAcknowledged = false;

			plist.SetBool(isAcknowledged, "IsAcknowledged");

			//System.Diagnostics.Debug.WriteLine("Resolution " + UIScreen.MainScreen.Bounds.Width + " " + UIScreen.MainScreen.Bounds.Width);

			return base.FinishedLaunching(app, options);
		}

		public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations(UIApplication application, [Transient] UIWindow forWindow)
		{
			return UIInterfaceOrientationMask.Landscape;
		}
	}
}

