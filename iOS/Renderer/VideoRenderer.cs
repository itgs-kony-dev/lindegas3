using System;

using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using System.IO;

using UIKit;
using CoreAnimation;
using Foundation;
using OpenGLES;
using ObjCRuntime;

using MediaPlayer;
using AVFoundation;

using LindeGas3;

using CoreGraphics;
using AVKit;



[assembly: ExportRenderer(typeof(VideoView), typeof(LindeGas3.iOS.VideoViewRenderer))]
namespace LindeGas3.iOS
{
	public class VideoViewRenderer : ViewRenderer<VideoView, UIView>, IDisposable
	{
		MPMoviePlayerController moviePlayer;

		public VideoViewRenderer()
		{
			
		}

		protected void Initialize(string url)
		{
			moviePlayer = new MPMoviePlayerController(NSUrl.FromFilename(url));

			moviePlayer.SetFullscreen(false, false);
			moviePlayer.Play();

			SetNativeControl(moviePlayer.View);
		}

		protected override void OnElementChanged(ElementChangedEventArgs<VideoView> e)
		{
			base.OnElementChanged(e);

			VideoView oldElement = e.OldElement;
			VideoView newElement = e.NewElement;

			if (newElement != null)
			{
				if (Control == null && !string.IsNullOrWhiteSpace(newElement.FileName))
					Initialize(newElement.FileName);
				
			}

			//NativeView.Add(pageControl);
		}

		protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);

			if (e.PropertyName == "FileName")
			{
				if (moviePlayer != null)
					moviePlayer.Dispose();

				if (!string.IsNullOrWhiteSpace(Element.FileName))
					Initialize(Element.FileName);
			}
		}

		protected override void Dispose(bool disposing)
		{
			moviePlayer?.Dispose();

			base.Dispose(disposing);
		}
	}
}
