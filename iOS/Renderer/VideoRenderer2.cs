using System;

using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using System.IO;

using UIKit;
using CoreAnimation;
using Foundation;
using OpenGLES;
using ObjCRuntime;

using MediaPlayer;
using AVFoundation;

using LindeGas3;

using CoreGraphics;
using AVKit;



[assembly: ExportRenderer(typeof(VideoView2), typeof(LindeGas3.iOS.VideoViewRenderer2))]
namespace LindeGas3.iOS
{
	public class VideoViewRenderer2 : ViewRenderer<VideoView2, UIView>, IDisposable
	{
		//globally declare variables
		AVAsset _asset;
		AVPlayerItem _playerItem;
		AVPlayer _player;

		AVPlayerLayer _playerLayer;
		UIButton playButton;

		protected void Initialize(string url)
		{
			try
			{
				_asset = AVAsset.FromUrl(NSUrl.FromFilename(url));
				_playerItem = new AVPlayerItem(_asset);

				//_playerItem = new AVPlayerItem (_asset);

				_player = new AVPlayer(_playerItem);

				_playerLayer = AVPlayerLayer.FromPlayer(_player);

				//Create the play button


				//Set the trigger on the play button to play the video
				//playButton.TouchUpInside += (object sender, EventArgs arg) =>
				//{
				//	_player.Play();
				//};

				//_playerLayer.Frame = NativeView.Frame;
				//NativeView.Layer.AddSublayer(_playerLayer);
				//NativeView.Add(playButton);

				//_playerLayer.Frame = NativeView.Frame;

				UIView view = new UIView();

				view.Bounds = NativeView.Frame;
				view.Layer.AddSublayer(_playerLayer);

				SetNativeControl(view);

				_player.Play();
			}
			catch (Exception exception)
			{
				Console.WriteLine("Exception: " + exception.Message);
			}
		}

		/*public override void LayoutSubviews()
		{
			base.LayoutSubviews();

			//layout the elements depending on what screen orientation we are. 
			/*if (DeviceHelper.iOSDevice.Orientation == UIDeviceOrientation.Portrait)
			{
				playButton.Frame = new CGRect(0, NativeView.Frame.Bottom - 50, NativeView.Frame.Width, 50);
				_playerLayer.Frame = NativeView.Frame;
				NativeView.Layer.AddSublayer(_playerLayer);
				NativeView.Add(playButton);
			}
			else if (DeviceHelper.iOSDevice.Orientation == UIDeviceOrientation.LandscapeLeft || DeviceHelper.iOSDevice.Orientation == UIDeviceOrientation.LandscapeRight)
			{
				_playerLayer.Frame = NativeView.Frame;
				NativeView.Layer.AddSublayer(_playerLayer);
				playButton.Frame = new CGRect(400, 400, 400, 400);
			//}
		}
		*/
		
		protected override void OnElementChanged(ElementChangedEventArgs<VideoView2> e)
		{
			try {
				base.OnElementChanged(e);

				VideoView2 oldElement = e.OldElement;
				VideoView2 newElement = e.NewElement;

				if (newElement != null)
				{
					if (Control == null && !string.IsNullOrWhiteSpace(newElement.FileName))
						Initialize(newElement.FileName);
					
				}

			}
			catch (Exception exception)
			{

			}

			//NativeView.Add(pageControl);
		}

		protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			try
			{
				base.OnElementPropertyChanged(sender, e);

				if (e.PropertyName == "FileName")
				{
					if (_playerItem != null)
						_playerItem.Dispose();

					if (!string.IsNullOrWhiteSpace(Element.FileName))
						Initialize(Element.FileName);
				}
			}
			catch (Exception exception)
			{

			}
		}

		public override void LayoutSubviews()
		{
			try
			{
				base.LayoutSubviews();

				//layout the elements depending on what screen orientation we are. 
				//if (DeviceHelper.iOSDevice.Orientation == UIDeviceOrientation.Portrait) {
				//playButton.Frame = new CGRect(0, NativeView.Frame.Bottom - 50, NativeView.Frame.Width, 50);
				_playerLayer.Frame = NativeView.Frame;
				NativeView.Layer.AddSublayer(_playerLayer);
				//NativeView.Add(playButton);
				/*} else if (DeviceHelper.iOSDevice.Orientation == UIDeviceOrientation.LandscapeLeft || DeviceHelper.iOSDevice.Orientation == UIDeviceOrientation.LandscapeRight) {

					_playerLayer.Frame = NativeView.Frame;
					NativeView.Layer.AddSublayer (_playerLayer);
					playButton.Frame = new CGRect (0, 0, 0, 0);
				}*/
			}
			catch (Exception exception)
			{
				
			}
		}

		protected override void Dispose(bool disposing)
		{
			_playerItem?.Dispose();

			base.Dispose(disposing);
		}


	}
}
