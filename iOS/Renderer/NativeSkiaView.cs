using System;
using CoreGraphics;
using UIKit;
using SkiaSharp;

namespace LindeGas3.iOS
{
	public class NativeSkiaView: UIView
	{
		const int bitmapInfo = ((int)CGBitmapFlags.ByteOrder32Big) | ((int)CGImageAlphaInfo.PremultipliedLast);

		ISkiaViewController skiaView;

		public NativeSkiaView (SkiaView skiaView)
		{
			this.skiaView = skiaView;

			AddGestureRecognizer (new UITapGestureRecognizer (OnTapped));
		}

		public override void Draw (CGRect rect)
		{
			base.Draw (rect);

			var screenScale = UIScreen.MainScreen.Scale;
			//var width = (int)(rect.Width * screenScale * 2);
			//var height = (int)(rect.Height * screenScale * 2);
			//var width = (int)(Bounds.Width * screenScale);
			//var height = (int)(Bounds.Height * screenScale);
			int width = (int) ((720 * (Bounds.Height > Bounds.Width ? 1.44 : 2.64)) * screenScale / 2);
			int height = (int) (width * (Bounds.Height / Bounds.Width));

			IntPtr buff = System.Runtime.InteropServices.Marshal.AllocCoTaskMem (width * height * 4);
			try {
				using (var surface = SKSurface.Create (width, height, SKImageInfo.PlatformColorType, SKAlphaType.Premul, buff, width * 4)) {
					var skcanvas = surface.Canvas;
					skcanvas.Scale ((float)screenScale, (float)screenScale);
					using (new SKAutoCanvasRestore (skcanvas, true)) {
						skiaView.SendDraw (skcanvas);
					}
				}

				using (var colorSpace = CGColorSpace.CreateDeviceRGB ())
				using (var bContext = new CGBitmapContext (buff, width, height, 8, width * 4, colorSpace, (CGImageAlphaInfo)bitmapInfo))
				using (var image = bContext.ToImage ())
				using (var context = UIGraphics.GetCurrentContext ()) {
					// flip the image for CGContext.DrawImage
					context.TranslateCTM (0, Frame.Height);
					context.ScaleCTM (1, -1);
					context.DrawImage (Bounds, image);
				}
			} finally {
				if (buff != IntPtr.Zero)
					System.Runtime.InteropServices.Marshal.FreeCoTaskMem (buff);
			}
		}

		void OnTapped ()
		{
			skiaView.SendTap ();
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			SetNeedsDisplay ();
		}
	}
}

