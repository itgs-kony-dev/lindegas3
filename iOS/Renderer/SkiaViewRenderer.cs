using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer (typeof(LindeGas3.SkiaView), typeof(LindeGas3.iOS.SkiaViewRenderer))]

namespace LindeGas3.iOS
{
	public class SkiaViewRenderer: ViewRenderer<SkiaView, NativeSkiaView>
	{
		public SkiaViewRenderer ()
		{
		}

		protected override void OnElementChanged (ElementChangedEventArgs<SkiaView> e)
		{
			base.OnElementChanged (e);

			if (Control == null)
				SetNativeControl (new NativeSkiaView (Element));
		}
	}
}

