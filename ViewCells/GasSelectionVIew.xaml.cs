﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace LindeGas3
{
	public partial class GasSelectionVIew : ContentView
	{
		public static readonly BindableProperty GasProperty = BindableProperty.Create("Gas", typeof(Gas), typeof(GasSelectionVIew), new Gas(), propertyChanging: (x, o, n) => ((GasSelectionVIew)x).OnGasChanged(x, o, n));
		public static readonly BindableProperty ParentListProperty = BindableProperty.Create("ParentList", typeof(string), typeof(GasSelectionVIew), "");


		public Gas Gas
		{
			get { return (Gas)GetValue(GasProperty); }
			set { SetValue(GasProperty, value); }
		}
	
		public string ParentList
		{
			get { return (string)GetValue(ParentListProperty); }
			set { SetValue(ParentListProperty, value); }
		}

		public event EventHandler Clicked;

		public GasSelectionVIew()
		{
			InitializeComponent();

			this.SetBinding(GasProperty, new Binding("Gas", BindingMode.TwoWay));
			this.SetBinding(ParentListProperty, new Binding("ParentList", BindingMode.TwoWay));
		}

		protected void OnGasTapped(object sender, EventArgs e)
		{
			if (Gas != null)
			{
				Gas.IsSelected = !Gas.IsSelected;
				SendClicked();
			}
		}

		protected void OnGasChanged(BindableObject bindable, object oldValue, object newValue)
		{
			//Gas gas = (Gas)newValue;

			//BindingContext = gas;

			//this.nameLabel.Text = gas?.Name;
		}

		protected override void OnBindingContextChanged()
		{
			base.OnBindingContextChanged();

			if (BindingContext is Gas)
			{
				Gas = (Gas)BindingContext;
			}
		}

		void SendClicked()
		{
			/*ICommand cmd = Command;
			if (cmd != null)
				cmd.Execute(CommandParameter);*/

			EventHandler handler = Clicked;

			if (handler != null)
				handler(this, EventArgs.Empty);
		}
	}
}
