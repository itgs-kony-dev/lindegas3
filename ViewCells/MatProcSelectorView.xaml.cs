﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Windows.Input;

using Xamarin;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Platform;

namespace LindeGas3
{
	public partial class MatProcSelectorView : ContentView
	{
		public static readonly BindableProperty MaterialSelectedProperty = BindableProperty.Create("MaterialSelected", typeof(bool), typeof(MatProcSelectorView), false/*, propertyChanged: OnMaterialSelectionChanged*/);
		public static readonly BindableProperty GMAWSelectedProperty = BindableProperty.Create("GMAWSelected", typeof(bool), typeof(MatProcSelectorView), false/*, propertyChanged: OnMaterialSelectionChanged*/);
		public static readonly BindableProperty GTAWSelectedProperty = BindableProperty.Create("GTAWSelected", typeof(bool), typeof(MatProcSelectorView), false/*, propertyChanged: OnMaterialSelectionChanged*/);
		public static readonly BindableProperty CorgonSelectedProperty = BindableProperty.Create("CorgonSelected", typeof(bool), typeof(MatProcSelectorView), false/*, propertyChanged: OnMaterialSelectionChanged*/);
		public static readonly BindableProperty MisonSelectedProperty = BindableProperty.Create("MisonSelected", typeof(bool), typeof(MatProcSelectorView), false/*, propertyChanged: OnMaterialSelectionChanged*/);


		public static readonly BindableProperty CommandProperty = BindableProperty.Create("Command", typeof(ICommand), typeof(MatProcSelectorView), null, propertyChanged: (bo, o, n) => ((MatProcSelectorView)bo).OnCommandChanged());

		public static readonly BindableProperty CommandParameterProperty = BindableProperty.Create("CommandParameter", typeof(object), typeof(MatProcSelectorView), null, propertyChanged: (bindable, oldvalue, newvalue) => ((MatProcSelectorView)bindable).CommandCanExecuteChanged(bindable, EventArgs.Empty));


		public ICommand Command
		{
			get { return (ICommand)GetValue(CommandProperty); }
			set { SetValue(CommandProperty, value); }
		}

		public object CommandParameter
		{
			get { return GetValue(CommandParameterProperty); }
			set { SetValue(CommandParameterProperty, value); }
		}

		public bool MaterialSelected
		{
			get { return (bool) GetValue(MaterialSelectedProperty); }
			set { SetValue(MaterialSelectedProperty, value); }
		}

		public bool GMAWSelected
		{
			get { return (bool)GetValue(GMAWSelectedProperty); }
			set { SetValue(GMAWSelectedProperty, value); }
		}

		public bool GTAWSelected
		{
			get { return (bool)GetValue(GTAWSelectedProperty); }
			set { SetValue(GTAWSelectedProperty, value); }
		}

		public bool CorgonSelected
		{
			get { return (bool)GetValue(CorgonSelectedProperty); }
			set { SetValue(CorgonSelectedProperty, value); }
		}

		public bool MisonSelected
		{
			get { return (bool)GetValue(MisonSelectedProperty); }
			set { SetValue(MisonSelectedProperty, value); }
		}

		public event EventHandler Clicked;


		public MatProcSelectorView()
		{
			InitializeComponent();

			this.SetBinding(MaterialSelectedProperty, new Binding("MaterialSelected", BindingMode.TwoWay));
			this.SetBinding(GMAWSelectedProperty, new Binding("GMAWSelected", BindingMode.TwoWay));
			this.SetBinding(GTAWSelectedProperty, new Binding("GTAWSelected", BindingMode.TwoWay));
			this.SetBinding(CorgonSelectedProperty, new Binding("CorgonSelected", BindingMode.TwoWay));
			this.SetBinding(MisonSelectedProperty, new Binding("MisonSelected", BindingMode.TwoWay));
		}

		public void OnMaterialTapped(object sender, EventArgs args)
		{
			MaterialSelected = !MaterialSelected;

			if (!MaterialSelected)
			{
				GMAWSelected = false;
				GTAWSelected = false;
				CorgonSelected = false;
				MisonSelected = false;
			}

			SendClicked();
		}

		public void OnGMAWTapped(object sender, EventArgs args)
		{
			GMAWSelected = !GMAWSelected;

			SendClicked();
		}

		public void OnGTAWTapped(object sender, EventArgs args)
		{
			GTAWSelected = !GTAWSelected;

			SendClicked();
		}

		public void OnCorgonTapped(object sender, EventArgs args)
		{
			CorgonSelected = !CorgonSelected;

			SendClicked();
		}

		public void OnMisonTapped(object sender, EventArgs args)
		{
			MisonSelected = !MisonSelected;

			SendClicked();
		}

		void SendClicked()
		{
			ICommand cmd = Command;
			if (cmd != null)
				cmd.Execute(CommandParameter);

			EventHandler handler = Clicked;

			if (handler != null)
				handler(this, EventArgs.Empty);
		}


		void OnCommandChanged()
		{
			if (Command != null)
			{
				Command.CanExecuteChanged += CommandCanExecuteChanged;
				CommandCanExecuteChanged(this, EventArgs.Empty);
			}
		}

		void CommandCanExecuteChanged(object sender, EventArgs eventArgs)
		{
			ICommand cmd = Command;
			if (cmd != null)
				/*IsEnabledCore =*/ cmd.CanExecute(CommandParameter);
		}

	}
}
