﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace LindeGas3
{
	public partial class FumeFormationView : ContentView
	{
		public static readonly BindableProperty FFRProperty = BindableProperty.Create("FFR", typeof(double), typeof(FumeFormationView), 0d, propertyChanging: (x, o, n) => ((FumeFormationView)x).OnFFRChanged(x, o, n));

		public double FFR
		{
			get { return (double)GetValue(FFRProperty); }
			set { SetValue(FFRProperty, value); }
		}

		public FumeFormationView()
		{
			InitializeComponent();

			this.SetBinding(FFRProperty, new Binding("FFR", BindingMode.TwoWay));
		}

		protected void OnFFRChanged(BindableObject bindable, object oldValue, object newValue)
		{
			double dOld = (double) oldValue;
			double dValue = (double) newValue;

			this.ffrLabel.Text = dValue.ToString();

			Animation animation = new Animation(v =>
			{
				this.ffrLabel.Text = v.ToString("F2");
				AbsoluteLayout.SetLayoutBounds(this.ffrLabel, new Rectangle(0.090 + (1d / 13.25d) * v, 1, 0.160, 0.3));
				AbsoluteLayout.SetLayoutBounds(this.ffrImage, new Rectangle(-0.070 + (1d / 13.25d) * v, 1, 0.070, 1));
			}, dOld, dValue);
			animation.Commit(this, "SimpleAnimation", 50, 1000, Easing.CubicInOut, null/*(v, c) => this.chartBoxView.HeightRequest = (dValue <= double.Epsilon ? 0 : (Value / dValue) * 50)*/, () => false);

			//AbsoluteLayout.SetLayoutFlags(this.ffrImage, AbsoluteLayoutFlags.All);
			//AbsoluteLayout.SetLayoutBounds(this.ffrImage, new Rectangle(0.020 + (1d / 14.6d) * dValue, 1, 0.020, 1));
		}
	}
}
