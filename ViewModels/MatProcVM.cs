﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Input;

using Xamarin.Forms;

namespace LindeGas3
{
	public class StateEventArgs : EventArgs
	{
		public bool State { get; private set; }
		public string PropertyName { get; private set; }

		public StateEventArgs(bool state, string propertyName = "")
		{
			this.State = state;
			this.PropertyName = propertyName;
		}
	}

	public delegate void OnSelectionChangedHandler(object sender, StateEventArgs args);

	public class MatProcVM : ModelBase
	{
		public event OnSelectionChangedHandler OnSelected;

		//public ICommand MatProcCommand { get; private set; }

		Metal material;
		bool materialSelected;
		bool gmawSelected;
		bool gtawSelected;
		bool corgonSelected;
		bool misonSelected;
		bool hasCorgon;

		public Metal Material
		{
			set
			{
				this.material = value;
				OnPropertyChanged("Material");
			}
			get { return this.material; }
		}

		public string MaterialName
		{
			get { return this.material?.Name.ToUpper(); }
		}

		public bool MaterialSelected
		{
			set
			{
				this.materialSelected = value;
				OnPropertyChanged("MaterialSelected");

				if (OnSelected != null)
					OnSelected(this, new StateEventArgs(value, "MaterialSelected"));

			}
			get { return this.materialSelected; }
		}

		public bool GMAWSelected
		{
			set
			{
				if (this.gmawSelected != value)
				{
					this.gmawSelected = value;
					OnPropertyChanged("GMAWSelected");

					if (OnSelected != null)
						OnSelected(this, new StateEventArgs(value, "GMAWSelected"));
				}

			}
			get { return this.gmawSelected; }
		}

		public bool GTAWSelected
		{
			set
			{
				if (this.gtawSelected != value)
				{
					this.gtawSelected = value;
					OnPropertyChanged("GTAWSelected");

					if (OnSelected != null)
						OnSelected(this, new StateEventArgs(value, "GTAWSelected"));
				}

			}
			get { return this.gtawSelected; }
		}

		public bool HasCorgon
		{
			set
			{
				this.hasCorgon = value;
				OnPropertyChanged("HasCorgon");

				if (OnSelected != null)
					OnSelected(this, new StateEventArgs(value, "HasCorgon"));

			}
			get { return this.hasCorgon; }
		}

		public bool CorgonSelected
		{
			set
			{
				if (this.corgonSelected != value)
				{
					this.corgonSelected = value;
					OnPropertyChanged("CorgonSelected");

					if (OnSelected != null)
						OnSelected(this, new StateEventArgs(value, "CorgonSelected"));
				}

			}
			get { return this.corgonSelected; }
		}

		public bool MisonSelected
		{
			set
			{
				if (this.misonSelected != value)
				{
					this.misonSelected = value;
					OnPropertyChanged("MisonSelected");

					if (OnSelected != null)
						OnSelected(this, new StateEventArgs(value, "MisonSelected"));
				}

			}
			get { return this.misonSelected; }
		}

		public void Unselect()
		{
			if(MaterialSelected)
				MaterialSelected = false;

			if (GMAWSelected)
				GMAWSelected = false;

			if (GTAWSelected)
				GTAWSelected = false;

			if (CorgonSelected)
				CorgonSelected = false;

			if (MisonSelected)
				MisonSelected = false;
		}
	}
}
